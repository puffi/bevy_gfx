use std::ffi::CString;

use bevy_app::{App, Events, Plugin};
use bevy_log::error;
use gfx::common::{Platform, PlatformHandle};
use sdl2_sys::sdl::{
    error::SDL_GetError,
    event::{
        SDL_Event, SDL_PollEvent, SDL_KEYDOWN, SDL_KEYUP, SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP,
        SDL_MOUSEMOTION, SDL_MOUSEWHEEL, SDL_QUIT, SDL_WINDOWEVENT,
    },
    syswm::{SDL_GetWindowWMInfo, SDL_SysWMinfo, SDL_SYSWM_TYPE},
    video::{
        SDL_CreateWindow, SDL_SetWindowTitle, SDL_Window, SDL_WINDOWEVENT_CLOSE,
        SDL_WINDOWEVENT_ENTER, SDL_WINDOWEVENT_EXPOSED, SDL_WINDOWEVENT_FOCUS_GAINED,
        SDL_WINDOWEVENT_FOCUS_LOST, SDL_WINDOWEVENT_HIDDEN, SDL_WINDOWEVENT_HIT_TEST,
        SDL_WINDOWEVENT_LEAVE, SDL_WINDOWEVENT_MAXIMIZED, SDL_WINDOWEVENT_MINIMIZED,
        SDL_WINDOWEVENT_MOVED, SDL_WINDOWEVENT_NONE, SDL_WINDOWEVENT_RESIZED,
        SDL_WINDOWEVENT_RESTORED, SDL_WINDOWEVENT_SHOWN, SDL_WINDOWEVENT_SIZE_CHANGED,
        SDL_WINDOWEVENT_TAKE_FOCUS, SDL_WINDOW_SHOWN,
    },
    SDL_Init, SDL_bool, SDL_INIT_VIDEO,
};

use keycode::SDL_Keycode;
use scancode::SDL_Scancode;
pub use sdl2_sys::sdl::keycode;
pub use sdl2_sys::sdl::mouse;
pub use sdl2_sys::sdl::scancode;

#[derive(Default)]
pub struct SDL2WindowPlugin;

impl Plugin for SDL2WindowPlugin {
    fn build(&self, app: &mut App) {
        app.insert_non_send_resource(Window::new("bevy_gfx"))
            .set_runner(sdl_event_loop)
            .add_event::<WindowEvent>()
            .add_event::<KeyEvent>()
            .add_event::<MouseButtonEvent>()
            .add_event::<MouseMoveEvent>()
            .add_event::<MouseWheelEvent>();
    }
}

pub struct Window {
    pub win: *mut SDL_Window,
    title: String,
    size: (i32, i32),
    is_open: bool,
    has_focus: bool,
    is_cursor_inside: bool,
}

impl Window {
    pub fn new<S: AsRef<str>>(title: S) -> Self {
        assert!(unsafe { SDL_Init(SDL_INIT_VIDEO) } == 0);

        let c_title = CString::new(title.as_ref()).unwrap();
        let win = unsafe { SDL_CreateWindow(c_title.as_ptr(), 0, 0, 1024, 768, SDL_WINDOW_SHOWN) };
        let size = (1024, 768);
        let is_open = true;

        Self {
            win,
            title: title.as_ref().to_string(),
            size,
            is_open,
            has_focus: false,
            is_cursor_inside: false,
        }
    }

    pub fn get_size(&self) -> (i32, i32) {
        self.size
    }

    pub fn is_open(&self) -> bool {
        self.is_open
    }

    pub fn has_focus(&self) -> bool {
        self.has_focus
    }

    pub fn is_cursor_inside(&self) -> bool {
        self.is_cursor_inside
    }

    pub fn get_title(&self) -> &str {
        &self.title
    }

    pub fn set_title<S: AsRef<str>>(&mut self, title: S) {
        self.title = title.as_ref().to_string();
        let c_title = CString::new(title.as_ref()).unwrap();
        unsafe {
            SDL_SetWindowTitle(self.win, c_title.as_ptr());
        }
    }
}

fn sdl_event_loop(mut app: App) {
    let mut is_open = app
        .world
        .get_non_send_resource_mut::<Window>()
        .unwrap()
        .is_open;
    let mut evt = SDL_Event::default();
    while is_open {
        app.update();

        while unsafe { SDL_PollEvent(&mut evt as _) } != 0 {
            match unsafe { evt.ty } {
                SDL_QUIT => {
                    is_open = false;
                    app.world
                        .get_non_send_resource_mut::<Window>()
                        .unwrap()
                        .is_open = false;
                }
                SDL_WINDOWEVENT => match unsafe { evt.window.event } {
                    SDL_WINDOWEVENT_NONE => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::None);
                    }
                    SDL_WINDOWEVENT_SHOWN => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Shown);
                    }
                    SDL_WINDOWEVENT_HIDDEN => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Hidden);
                    }
                    SDL_WINDOWEVENT_EXPOSED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Exposed);
                    }
                    SDL_WINDOWEVENT_MOVED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        unsafe {
                            evts.send(WindowEvent::Moved(evt.window.data1, evt.window.data2));
                        }
                    }
                    SDL_WINDOWEVENT_RESIZED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        unsafe {
                            evts.send(WindowEvent::Resized(evt.window.data1, evt.window.data2));
                        }
                    }
                    SDL_WINDOWEVENT_SIZE_CHANGED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::SizeChanged);
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .size = unsafe { (evt.window.data1, evt.window.data2) };
                    }
                    SDL_WINDOWEVENT_MINIMIZED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Minimized);
                    }
                    SDL_WINDOWEVENT_MAXIMIZED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Maximized);
                    }
                    SDL_WINDOWEVENT_RESTORED => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Restored);
                    }
                    SDL_WINDOWEVENT_ENTER => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_cursor_inside = true;

                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Enter);
                    }
                    SDL_WINDOWEVENT_LEAVE => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_cursor_inside = false;
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Leave);
                    }
                    SDL_WINDOWEVENT_FOCUS_GAINED => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .has_focus = true;
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::FocusGained);
                    }
                    SDL_WINDOWEVENT_FOCUS_LOST => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .has_focus = false;
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::FocusLost);
                    }
                    SDL_WINDOWEVENT_CLOSE => {
                        is_open = false;
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_open = false;

                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::Close);
                    }
                    SDL_WINDOWEVENT_TAKE_FOCUS => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::TakeFocus);
                    }
                    SDL_WINDOWEVENT_HIT_TEST => {
                        let mut evts = app.world.get_resource_mut::<Events<WindowEvent>>().unwrap();
                        evts.send(WindowEvent::HitTest);
                    }
                    _ => {
                        error!("Unhandled window event: {:X}", unsafe { evt.ty });
                    }
                },
                SDL_KEYDOWN => {
                    let mut evts = app.world.get_resource_mut::<Events<KeyEvent>>().unwrap();
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Pressed,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    evts.send(key_evt);
                }
                SDL_KEYUP => {
                    let mut evts = app.world.get_resource_mut::<Events<KeyEvent>>().unwrap();
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Released,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    evts.send(key_evt);
                }
                SDL_MOUSEBUTTONDOWN => {
                    let mut evts = app
                        .world
                        .get_resource_mut::<Events<MouseButtonEvent>>()
                        .unwrap();
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Pressed,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    evts.send(mouse_btn_evt);
                }
                SDL_MOUSEBUTTONUP => {
                    let mut evts = app
                        .world
                        .get_resource_mut::<Events<MouseButtonEvent>>()
                        .unwrap();
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Released,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    evts.send(mouse_btn_evt);
                }
                SDL_MOUSEMOTION => {
                    let mut evts = app
                        .world
                        .get_resource_mut::<Events<MouseMoveEvent>>()
                        .unwrap();
                    let mouse_move_evt = unsafe {
                        MouseMoveEvent {
                            x: evt.motion.x,
                            y: evt.motion.y,
                            x_rel: evt.motion.xrel,
                            y_rel: evt.motion.yrel,
                            state: evt.motion.state,
                        }
                    };
                    evts.send(mouse_move_evt);
                }
                SDL_MOUSEWHEEL => {
                    let mut evts = app
                        .world
                        .get_resource_mut::<Events<MouseWheelEvent>>()
                        .unwrap();
                    unsafe {
                        evts.send(MouseWheelEvent {
                            x: evt.wheel.x,
                            y: evt.wheel.y,
                            direction: evt.wheel.direction,
                        });
                    }
                }
                _ => {}
            }
        }
    }
}

pub enum WindowEvent {
    None,
    Shown,
    Hidden,
    Exposed,
    Moved(i32, i32),
    Resized(i32, i32),
    SizeChanged,
    Minimized,
    Maximized,
    Restored,
    Enter,       // mouse
    Leave,       // mouse
    FocusGained, // keyboard
    FocusLost,   // keyboard
    Close,
    TakeFocus,
    HitTest,
}

#[derive(Copy, Clone, Debug)]
pub struct KeyEvent {
    pub state: State,
    pub scancode: SDL_Scancode,
    pub key: SDL_Keycode,
    pub modifiers: u16,
    pub repeat: u8,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum State {
    Released,
    Pressed,
}

#[derive(Copy, Clone, Debug)]
pub struct MouseButtonEvent {
    pub state: State,
    pub button: u8,
    pub clicks: u8,
    pub x: i32,
    pub y: i32,
}

#[derive(Copy, Clone, Debug)]
pub struct MouseMoveEvent {
    pub x: i32,
    pub y: i32,
    pub x_rel: i32,
    pub y_rel: i32,
    pub state: u32,
}

#[derive(Copy, Clone, Debug)]
pub struct MouseWheelEvent {
    pub x: i32,
    pub y: i32,
    pub direction: u32,
}

impl Platform for Window {
    fn get_platform_handle(&self) -> gfx::common::PlatformHandle {
        let mut wmi: SDL_SysWMinfo = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };
        wmi.version.major = 2;
        wmi.version.minor = 0;
        wmi.version.patch = 15;
        if unsafe { SDL_GetWindowWMInfo(self.win, &mut wmi) } == SDL_bool::False {
            let err_msg = unsafe { std::ffi::CStr::from_ptr(SDL_GetError() as _) };
            panic!(
                "[Window]: Failed to get window WMInfo: {}",
                err_msg.to_str().unwrap()
            );
        }
        unsafe {
            match wmi.subsystem {
                SDL_SYSWM_TYPE::Windows => PlatformHandle::Win32 {
                    hwnd: wmi.info.win.window,
                    hdc: wmi.info.win.hdc,
                    hinstance: wmi.info.win.hinstance,
                },
                SDL_SYSWM_TYPE::X11 => PlatformHandle::Xlib {
                    window: wmi.info.x11.window,
                    display: wmi.info.x11.display,
                },
                SDL_SYSWM_TYPE::Wayland => PlatformHandle::Wayland {
                    surface: wmi.info.wl.surface,
                    display: wmi.info.wl.display,
                },
                _ => unimplemented!("{:?}", wmi.subsystem),
            }
        }
    }

    fn get_window_size(&self) -> (i32, i32) {
        self.get_size()
    }
}
