use bevy_app::Plugin;

pub mod gfx;
pub mod sdl2;

pub struct BevyGfxPlugin;

impl Plugin for BevyGfxPlugin {
    fn build(&self, app: &mut bevy_app::App) {
        app.add_plugin(sdl2::SDL2WindowPlugin);
        app.add_plugin(gfx::GfxPlugin);
    }
}
