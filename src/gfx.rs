use bevy_app::{App, Plugin};
use bevy_ecs::schedule::StageLabel;
use gfx::gfx::Gfx;

#[derive(Default)]
pub struct GfxPlugin;

impl Plugin for GfxPlugin {
    fn build(&self, app: &mut App) {
        let win = app
            .world
            .get_non_send_resource_mut::<super::sdl2::Window>()
            .unwrap();
        let mut gfx = Gfx::new();
        assert!(gfx.initialize(win.as_ref()));
        app.insert_non_send_resource(gfx);
    }
}

#[derive(StageLabel, Debug, PartialEq, Eq, Clone, Hash, Copy)]
pub enum GfxStage {
    Clear,
    Prepare,
    Draw,
    Present,
}
