#![allow(non_upper_case_globals)]
use super::platform::*;

pub type EGLint = i32;
pub type EGLBoolean = u32;
pub type EGLDisplay = *mut std::ffi::c_void;
pub type EGLConfig = *mut std::ffi::c_void;
pub type EGLSurface = *mut std::ffi::c_void;
pub type EGLContext = *mut std::ffi::c_void;

#[allow(non_camel_case_types)]
pub type __eglMustCastToProperFunctionPointerType = *const std::ffi::c_void;

pub const EGL_ALPHA_SIZE: i32 = 0x3021;
pub const EGL_BAD_ACCESS: i32 = 0x3002;
pub const EGL_BAD_ALLOC: i32 = 0x3003;
pub const EGL_BAD_ATTRIBUTE: i32 = 0x3004;
pub const EGL_BAD_CONFIG: i32 = 0x3005;
pub const EGL_BAD_CONTEXT: i32 = 0x3006;
pub const EGL_BAD_CURRENT_SURFACE: i32 = 0x3007;
pub const EGL_BAD_DISPLAY: i32 = 0x3008;
pub const EGL_BAD_MATCH: i32 = 0x3009;
pub const EGL_BAD_NATIVE_PIXMAP: i32 = 0x300A;
pub const EGL_BAD_NATIVE_WINDOW: i32 = 0x300B;
pub const EGL_BAD_PARAMETER: i32 = 0x300C;
pub const EGL_BAD_SURFACE: i32 = 0x300D;
pub const EGL_BLUE_SIZE: i32 = 0x3022;
pub const EGL_BUFFER_SIZE: i32 = 0x3020;
pub const EGL_CONFIG_CAVEAT: i32 = 0x3027;
pub const EGL_CONFIG_ID: i32 = 0x3028;
pub const EGL_CORE_NATIVE_ENGINE: i32 = 0x305B;
pub const EGL_DEPTH_SIZE: i32 = 0x3025;
pub const EGL_DONT_CARE: i32 = -1;
pub const EGL_DRAW: i32 = 0x3059;
pub const EGL_EXTENSIONS: i32 = 0x3055;
pub const EGL_FALSE: i32 = 0;
pub const EGL_GREEN_SIZE: i32 = 0x3023;
pub const EGL_HEIGHT: i32 = 0x3056;
pub const EGL_LARGEST_PBUFFER: i32 = 0x3058;
pub const EGL_LEVEL: i32 = 0x3029;
pub const EGL_MAX_PBUFFER_HEIGHT: i32 = 0x302A;
pub const EGL_MAX_PBUFFER_PIXELS: i32 = 0x302B;
pub const EGL_MAX_PBUFFER_WIDTH: i32 = 0x302C;
pub const EGL_NATIVE_RENDERABLE: i32 = 0x302D;
pub const EGL_NATIVE_VISUAL_ID: i32 = 0x302E;
pub const EGL_NATIVE_VISUAL_TYPE: i32 = 0x302F;
pub const EGL_NONE: i32 = 0x3038;
pub const EGL_NON_CONFORMANT_CONFIG: i32 = 0x3051;
pub const EGL_NOT_INITIALIZED: i32 = 0x3001;
pub const EGL_NO_CONTEXT: EGLContext = std::ptr::null_mut();
pub const EGL_NO_DISPLAY: EGLDisplay = std::ptr::null_mut();
pub const EGL_NO_SURFACE: EGLSurface = std::ptr::null_mut();
pub const EGL_PBUFFER_BIT: i32 = 0x0001;
pub const EGL_PIXMAP_BIT: i32 = 0x0002;
pub const EGL_READ: i32 = 0x305A;
pub const EGL_RED_SIZE: i32 = 0x3024;
pub const EGL_SAMPLES: i32 = 0x3031;
pub const EGL_SAMPLE_BUFFERS: i32 = 0x3032;
pub const EGL_SLOW_CONFIG: i32 = 0x3050;
pub const EGL_STENCIL_SIZE: i32 = 0x3026;
pub const EGL_SUCCESS: i32 = 0x3000;
pub const EGL_SURFACE_TYPE: i32 = 0x3033;
pub const EGL_TRANSPARENT_BLUE_VALUE: i32 = 0x3035;
pub const EGL_TRANSPARENT_GREEN_VALUE: i32 = 0x3036;
pub const EGL_TRANSPARENT_RED_VALUE: i32 = 0x3037;
pub const EGL_TRANSPARENT_RGB: i32 = 0x3052;
pub const EGL_TRANSPARENT_TYPE: i32 = 0x3034;
pub const EGL_TRUE: i32 = 1;
pub const EGL_VENDOR: i32 = 0x3053;
pub const EGL_VERSION: i32 = 0x3054;
pub const EGL_WIDTH: i32 = 0x3057;
pub const EGL_WINDOW_BIT: i32 = 0x0004;

#[link(name="EGL")]
extern "C" {
    pub fn eglChooseConfig(
        dpy: EGLDisplay,
        attrib_list: *const EGLint,
        configs: *mut EGLConfig,
        config_size: EGLint,
        num_config: *mut EGLint,
    ) -> EGLBoolean;

    pub fn eglCopyBuffers(
        dpy: EGLDisplay,
        surface: EGLSurface,
        target: EGLNativePixmapType,
    ) -> EGLBoolean;
    pub fn eglCreateContext(
        dpy: EGLDisplay,
        config: EGLConfig,
        share_context: EGLContext,
        attrib_list: *const EGLint,
    ) -> EGLContext;
    pub fn eglCreatePbufferSurface(
        dpy: EGLDisplay,
        config: EGLConfig,
        attrib_list: *const EGLint,
    ) -> EGLSurface;
    pub fn eglCreatePixmapSurface(
        dpy: EGLDisplay,
        config: EGLConfig,
        pixmap: EGLNativePixmapType,
        attrib_list: *const EGLint,
    ) -> EGLSurface;
    pub fn eglCreateWindowSurface(
        dpy: EGLDisplay,
        config: EGLConfig,
        win: EGLNativeWindowType,
        attrib_list: *const EGLint,
    ) -> EGLSurface;
    pub fn eglDestroyContext(dpy: EGLDisplay, ctx: EGLContext) -> EGLBoolean;
    pub fn eglDestroySurface(dpy: EGLDisplay, surface: EGLSurface) -> EGLBoolean;
    pub fn eglGetConfigAttrib(
        dpy: EGLDisplay,
        config: EGLConfig,
        attribute: EGLint,
        value: *mut EGLint,
    ) -> EGLBoolean;
    pub fn eglGetConfigs(
        dpy: EGLDisplay,
        configs: *mut EGLConfig,
        config_size: EGLint,
        num_config: *mut EGLint,
    ) -> EGLBoolean;
    pub fn eglGetCurrentDisplay() -> EGLDisplay;
    pub fn eglGetCurrentSurface(readdraw: EGLint) -> EGLSurface;
    pub fn eglGetDisplay(display_id: EGLNativeDisplayType) -> EGLDisplay;
    pub fn eglGetError() -> EGLint;
    pub fn eglGetProcAddress(
        procname: *const std::os::raw::c_char,
    ) -> __eglMustCastToProperFunctionPointerType;
    pub fn eglInitialize(dpy: EGLDisplay, major: *mut EGLint, minor: *mut EGLint) -> EGLBoolean;
    pub fn eglMakeCurrent(
        dpy: EGLDisplay,
        draw: EGLSurface,
        read: EGLSurface,
        ctx: EGLContext,
    ) -> EGLBoolean;
    pub fn eglQueryContext(
        dpy: EGLDisplay,
        ctx: EGLContext,
        attribute: EGLint,
        value: *mut EGLint,
    ) -> EGLBoolean;
    pub fn eglQueryString(dpy: EGLDisplay, name: EGLint) -> *const std::os::raw::c_char;
    pub fn eglQuerySurface(
        dpy: EGLDisplay,
        surface: EGLSurface,
        attribute: EGLint,
        value: *mut EGLint,
    ) -> EGLBoolean;
    pub fn eglSwapBuffers(dpy: EGLDisplay, surface: EGLSurface) -> EGLBoolean;
    pub fn eglTerminate(dpy: EGLDisplay) -> EGLBoolean;
    pub fn eglWaitGL() -> EGLBoolean;
    pub fn eglWaitNative(engine: EGLint) -> EGLBoolean;
}
// version 1.1

pub const EGL_BACK_BUFFER: i32 = 0x3084;
pub const EGL_BIND_TO_TEXTURE_RGB: i32 = 0x3039;
pub const EGL_BIND_TO_TEXTURE_RGBA: i32 = 0x303A;
pub const EGL_CONTEXT_LOST: i32 = 0x300E;
pub const EGL_MIN_SWAP_INTERVAL: i32 = 0x303B;
pub const EGL_MAX_SWAP_INTERVAL: i32 = 0x303C;
pub const EGL_MIPMAP_TEXTURE: i32 = 0x3082;
pub const EGL_MIPMAP_LEVEL: i32 = 0x3083;
pub const EGL_NO_TEXTURE: i32 = 0x305C;
pub const EGL_TEXTURE_2D: i32 = 0x305F;
pub const EGL_TEXTURE_FORMAT: i32 = 0x3080;
pub const EGL_TEXTURE_RGB: i32 = 0x305D;
pub const EGL_TEXTURE_RGBA: i32 = 0x305E;
pub const EGL_TEXTURE_TARGET: i32 = 0x3081;

#[link(name="EGL")]
extern "C" {
    pub fn eglBindTexImage(dpy: EGLDisplay, surface: EGLSurface, buffer: EGLint) -> EGLBoolean;
    pub fn eglReleaseTexImage(dpy: EGLDisplay, surface: EGLSurface, buffer: EGLint) -> EGLBoolean;
    pub fn eglSurfaceAttrib(
        dpy: EGLDisplay,
        surface: EGLSurface,
        attribute: EGLint,
        value: EGLint,
    ) -> EGLBoolean;
    pub fn eglSwapInterval(dpy: EGLDisplay, interval: EGLint) -> EGLBoolean;
}

// version 1.2

pub type EGLenum = u32;
pub type EGLClientBuffer = *mut std::ffi::c_void;

pub const EGL_ALPHA_FORMAT: i32 = 0x3088;
pub const EGL_ALPHA_FORMAT_NONPRE: i32 = 0x308B;
pub const EGL_ALPHA_FORMAT_PRE: i32 = 0x308C;
pub const EGL_ALPHA_MASK_SIZE: i32 = 0x303E;
pub const EGL_BUFFER_PRESERVED: i32 = 0x3094;
pub const EGL_BUFFER_DESTROYED: i32 = 0x3095;
pub const EGL_CLIENT_APIS: i32 = 0x308D;
pub const EGL_COLORSPACE: i32 = 0x3087;
pub const EGL_COLORSPACE_sRGB: i32 = 0x3089;
pub const EGL_COLORSPACE_LINEAR: i32 = 0x308A;
pub const EGL_COLOR_BUFFER_TYPE: i32 = 0x303F;
pub const EGL_CONTEXT_CLIENT_TYPE: i32 = 0x3097;
pub const EGL_DISPLAY_SCALING: i32 = 10000;
pub const EGL_HORIZONTAL_RESOLUTION: i32 = 0x3090;
pub const EGL_LUMINANCE_BUFFER: i32 = 0x308F;
pub const EGL_LUMINANCE_SIZE: i32 = 0x303D;
pub const EGL_OPENGL_ES_BIT: i32 = 0x0001;
pub const EGL_OPENVG_BIT: i32 = 0x0002;
pub const EGL_OPENGL_ES_API: i32 = 0x30A0;
pub const EGL_OPENVG_API: i32 = 0x30A1;
pub const EGL_OPENVG_IMAGE: i32 = 0x3096;
pub const EGL_PIXEL_ASPECT_RATIO: i32 = 0x3092;
pub const EGL_RENDERABLE_TYPE: i32 = 0x3040;
pub const EGL_RENDER_BUFFER: i32 = 0x3086;
pub const EGL_RGB_BUFFER: i32 = 0x308E;
pub const EGL_SINGLE_BUFFER: i32 = 0x3085;
pub const EGL_SWAP_BEHAVIOR: i32 = 0x3093;
pub const EGL_UNKNOWN: EGLint = 1;
pub const EGL_VERTICAL_RESOLUTION: i32 = 0x3091;

#[link(name="EGL")]
extern "C" {
    pub fn eglBindAPI(api: EGLenum) -> EGLBoolean;
    pub fn eglQueryAPI() -> EGLenum;
    pub fn eglCreatePbufferFromClientBuffer(
        dpy: EGLDisplay,
        buftype: EGLenum,
        buffer: EGLClientBuffer,
        config: EGLConfig,
        attrib_list: *const EGLint,
    ) -> EGLSurface;
    pub fn eglReleaseThread() -> EGLBoolean;
    pub fn eglWaitClient() -> EGLBoolean;
}

// version 1.3

pub const EGL_CONFORMANT: i32 = 0x3042;
pub const EGL_CONTEXT_CLIENT_VERSION: i32 = 0x3098;
pub const EGL_MATCH_NATIVE_PIXMAP: i32 = 0x3041;
pub const EGL_OPENGL_ES2_BIT: i32 = 0x0004;
pub const EGL_VG_ALPHA_FORMAT: i32 = 0x3088;
pub const EGL_VG_ALPHA_FORMAT_NONPRE: i32 = 0x308B;
pub const EGL_VG_ALPHA_FORMAT_PRE: i32 = 0x308C;
pub const EGL_VG_ALPHA_FORMAT_PRE_BIT: i32 = 0x0040;
pub const EGL_VG_COLORSPACE: i32 = 0x3087;
pub const EGL_VG_COLORSPACE_sRGB: i32 = 0x3089;
pub const EGL_VG_COLORSPACE_LINEAR: i32 = 0x308A;
pub const EGL_VG_COLORSPACE_LINEAR_BIT: i32 = 0x0020;

// version 1.4

pub const EGL_DEFAULT_DISPLAY: EGLNativeDisplayType = std::ptr::null_mut();
pub const EGL_MULTISAMPLE_RESOLVE_BOX_BIT: i32 = 0x0200;
pub const EGL_MULTISAMPLE_RESOLVE: i32 = 0x3099;
pub const EGL_MULTISAMPLE_RESOLVE_DEFAULT: i32 = 0x309A;
pub const EGL_MULTISAMPLE_RESOLVE_BOX: i32 = 0x309B;
pub const EGL_OPENGL_API: i32 = 0x30A2;
pub const EGL_OPENGL_BIT: i32 = 0x0008;
pub const EGL_SWAP_BEHAVIOR_PRESERVED_BIT: i32 = 0x0400;

#[link(name="EGL")]
extern "C" {
    pub fn eglGetCurrentContext() -> EGLContext;
}

// version 1.5

pub type EGLSync = *mut std::ffi::c_void;
pub type EGLAttrib = *mut i32;
pub type EGLTime = u64;
pub type EGLImage = *mut std::ffi::c_void;

pub const EGL_CONTEXT_MAJOR_VERSION: i32 = 0x3098;
pub const EGL_CONTEXT_MINOR_VERSION: i32 = 0x30FB;
pub const EGL_CONTEXT_OPENGL_PROFILE_MASK: i32 = 0x30FD;
pub const EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY: i32 = 0x31BD;
pub const EGL_NO_RESET_NOTIFICATION: i32 = 0x31BE;
pub const EGL_LOSE_CONTEXT_ON_RESET: i32 = 0x31BF;
pub const EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT: i32 = 0x00000001;
pub const EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT: i32 = 0x00000002;
pub const EGL_CONTEXT_OPENGL_DEBUG: i32 = 0x31B0;
pub const EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE: i32 = 0x31B1;
pub const EGL_CONTEXT_OPENGL_ROBUST_ACCESS: i32 = 0x31B2;
pub const EGL_OPENGL_ES3_BIT: i32 = 0x00000040;
pub const EGL_CL_EVENT_HANDLE: i32 = 0x309C;
pub const EGL_SYNC_CL_EVENT: i32 = 0x30FE;
pub const EGL_SYNC_CL_EVENT_COMPLETE: i32 = 0x30FF;
pub const EGL_SYNC_PRIOR_COMMANDS_COMPLETE: i32 = 0x30F0;
pub const EGL_SYNC_TYPE: i32 = 0x30F7;
pub const EGL_SYNC_STATUS: i32 = 0x30F1;
pub const EGL_SYNC_CONDITION: i32 = 0x30F8;
pub const EGL_SIGNALED: i32 = 0x30F2;
pub const EGL_UNSIGNALED: i32 = 0x30F3;
pub const EGL_SYNC_FLUSH_COMMANDS_BIT: i32 = 0x0001;
pub const EGL_FOREVER: u64 = 0xFFFFFFFFFFFFFFFF;
pub const EGL_TIMEOUT_EXPIRED: i32 = 0x30F5;
pub const EGL_CONDITION_SATISFIED: i32 = 0x30F6;
pub const EGL_NO_SYNC: EGLSync = std::ptr::null_mut();
pub const EGL_SYNC_FENCE: i32 = 0x30F9;
pub const EGL_GL_COLORSPACE: i32 = 0x309D;
pub const EGL_GL_COLORSPACE_SRGB: i32 = 0x3089;
pub const EGL_GL_COLORSPACE_LINEAR: i32 = 0x308A;
pub const EGL_GL_RENDERBUFFER: i32 = 0x30B9;
pub const EGL_GL_TEXTURE_2D: i32 = 0x30B1;
pub const EGL_GL_TEXTURE_LEVEL: i32 = 0x30BC;
pub const EGL_GL_TEXTURE_3D: i32 = 0x30B2;
pub const EGL_GL_TEXTURE_ZOFFSET: i32 = 0x30BD;
pub const EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X: i32 = 0x30B3;
pub const EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X: i32 = 0x30B4;
pub const EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y: i32 = 0x30B5;
pub const EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: i32 = 0x30B6;
pub const EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z: i32 = 0x30B7;
pub const EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: i32 = 0x30B8;
pub const EGL_IMAGE_PRESERVED: i32 = 0x30D2;
pub const EGL_NO_IMAGE: EGLImage = std::ptr::null_mut();

#[link(name="EGL")]
extern "C" {
    pub fn eglCreateSync(dpy: EGLDisplay, type_: EGLenum, attrib_list: *const EGLAttrib) -> EGLSync;
    pub fn eglDestroySync(dpy: EGLDisplay, sync: EGLSync) -> EGLBoolean;
    pub fn eglClientWaitSync(dpy: EGLDisplay, sync: EGLSync, flags: EGLint, timeout: EGLTime)
        -> EGLint;
    pub fn eglGetSyncAttrib(
        dpy: EGLDisplay,
        sync: EGLSync,
        attribute: EGLint,
        value: *mut EGLAttrib,
    ) -> EGLBoolean;
    pub fn eglCreateImage(
        dpy: EGLDisplay,
        ctx: EGLContext,
        target: EGLenum,
        buffer: EGLClientBuffer,
        attrib_list: *const EGLAttrib,
    ) -> EGLImage;
    pub fn eglDestroyImage(dpy: EGLDisplay, image: EGLImage) -> EGLBoolean;
    pub fn eglGetPlatformDisplay(
        platform: EGLenum,
        native_display: *mut std::ffi::c_void,
        attrib_list: *const EGLAttrib,
    ) -> EGLDisplay;
    pub fn eglCreatePlatformWindowSurface(
        dpy: EGLDisplay,
        config: EGLConfig,
        native_window: *mut std::ffi::c_void,
        attrib_list: *const EGLAttrib,
    ) -> EGLSurface;
    pub fn eglCreatePlatformPixmapSurface(
        dpy: EGLDisplay,
        config: EGLConfig,
        native_pixmap: *mut std::ffi::c_void,
        attrib_list: *const EGLAttrib,
    ) -> EGLSurface;
    pub fn eglWaitSync(dpy: EGLDisplay, sync: EGLSync, flags: EGLint) -> EGLBoolean;
}

// EXT

// EGL_KHR_create_context
pub const EGL_CONTEXT_MAJOR_VERSION_KHR: i32 = 0x3098;
pub const EGL_CONTEXT_MINOR_VERSION_KHR: i32 = 0x30FB;
pub const EGL_CONTEXT_FLAGS_KHR: i32 = 0x30FC;
pub const EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR: i32 = 0x30FD;
pub const EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_KHR: i32 = 0x31BD;
pub const EGL_NO_RESET_NOTIFICATION_KHR: i32 = 0x31BE;
pub const EGL_LOSE_CONTEXT_ON_RESET_KHR: i32 = 0x31BF;
pub const EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR: i32 = 0x00000001;
pub const EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE_BIT_KHR: i32 = 0x00000002;
pub const EGL_CONTEXT_OPENGL_ROBUST_ACCESS_BIT_KHR: i32 = 0x00000004;
pub const EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR: i32 = 0x00000001;
pub const EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT_KHR: i32 = 0x00000002;
pub const EGL_OPENGL_ES3_BIT_KHR: i32 = 0x00000040;
