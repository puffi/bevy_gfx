use std::os::raw::c_char;

pub type c_str = *const std::os::raw::c_char;

pub const SDL_WINDOW_FULLSCREEN: u32            = 0x00000001;
pub const SDL_WINDOW_OPENGL: u32                = 0x00000002;
pub const SDL_WINDOW_SHOWN: u32                 = 0x00000004;
pub const SDL_WINDOW_HIDDEN: u32                = 0x00000008;
pub const SDL_WINDOW_BORDERLESS: u32            = 0x00000010;
pub const SDL_WINDOW_RESIZABLE: u32             = 0x00000020;
pub const SDL_WINDOW_MINIMIZED: u32             = 0x00000040;
pub const SDL_WINDOW_MAXIMIZED: u32             = 0x00000080;
pub const SDL_WINDOW_MOUSE_GRABBED: u32         = 0x00000100;
pub const SDL_WINDOW_INPUT_FOCUS: u32           = 0x00000200;
pub const SDL_WINDOW_MOUSE_FOCUS: u32           = 0x00000400;
pub const SDL_WINDOW_FOREIGN: u32               = 0x00000800;
pub const SDL_WINDOW_FULLSCREEN_DESKTOP: u32    = 0x00001000 | SDL_WINDOW_FULLSCREEN;
pub const SDL_WINDOW_ALLOW_HIGHDPI: u32         = 0x00002000;
pub const SDL_WINDOW_MOUSE_CAPTURE: u32         = 0x00004000;
pub const SDL_WINDOW_ALWAYS_ON_TOP: u32         = 0x00008000;
pub const SDL_WINDOW_SKIP_TASKBAR: u32          = 0x00010000;
pub const SDL_WINDOW_UTILITY: u32               = 0x00020000;
pub const SDL_WINDOW_TOOLTIP: u32               = 0x00040000;
pub const SDL_WINDOW_POPUP_MENU: u32            = 0x00080000;
pub const SDL_WINDOW_KEYBOARD_GRABBED: u32      = 0x00100000;
pub const SDL_WINDOW_VULKAN: u32                = 0x10000000;
pub const SDL_WINDOW_METAL: u32                 = 0x20000000;

pub const SDL_WINDOW_INPUT_GRABBED: u32 = SDL_WINDOW_MOUSE_GRABBED;

pub type SDL_WindowEventID = u8;
pub const SDL_WINDOWEVENT_NONE: SDL_WindowEventID = 0;           /**< Never used */
pub const SDL_WINDOWEVENT_SHOWN: SDL_WindowEventID = 1;          /**< Window has been shown */
pub const SDL_WINDOWEVENT_HIDDEN: SDL_WindowEventID = 2;         /**< Window has been hidden */
pub const SDL_WINDOWEVENT_EXPOSED: SDL_WindowEventID = 3;        /**< Window has been exposed and should be
                                                                   redrawn */
pub const SDL_WINDOWEVENT_MOVED: SDL_WindowEventID = 4;          /**< Window has been moved to data1, data2
*/
pub const SDL_WINDOWEVENT_RESIZED: SDL_WindowEventID = 5;        /**< Window has been resized to data1xdata2 */
pub const SDL_WINDOWEVENT_SIZE_CHANGED: SDL_WindowEventID = 6;   /**< The window size has changed, either as
                                                                   a result of an API call or through the
                                                                   system or user changing the window size. */
pub const SDL_WINDOWEVENT_MINIMIZED: SDL_WindowEventID = 7;      /**< Window has been minimized */
pub const SDL_WINDOWEVENT_MAXIMIZED: SDL_WindowEventID = 8;      /**< Window has been maximized */
pub const SDL_WINDOWEVENT_RESTORED: SDL_WindowEventID = 9;       /**< Window has been restored to normal size
                                                                   and position */
pub const SDL_WINDOWEVENT_ENTER: SDL_WindowEventID = 10;          /**< Window has gained mouse focus */
pub const SDL_WINDOWEVENT_LEAVE: SDL_WindowEventID = 11;          /**< Window has lost mouse focus */
pub const SDL_WINDOWEVENT_FOCUS_GAINED: SDL_WindowEventID = 12;   /**< Window has gained keyboard focus */
pub const SDL_WINDOWEVENT_FOCUS_LOST: SDL_WindowEventID = 13;     /**< Window has lost keyboard focus */
pub const SDL_WINDOWEVENT_CLOSE: SDL_WindowEventID = 14;          /**< The window manager requests that the window be closed */
pub const SDL_WINDOWEVENT_TAKE_FOCUS: SDL_WindowEventID = 15;     /**< Window is being offered a focus (should SetWindowInputFocus() on itself or a subwindow, or ignore) */
pub const SDL_WINDOWEVENT_HIT_TEST: SDL_WindowEventID = 16;        /**< Window had a hit test that wasn't SDL_HITTEST_NORMAL. */

#[repr(C)]
pub struct SDL_Window {
    _private: [u8; 0],
}

extern {
    pub fn SDL_CreateWindow(title: c_str, x: i32, y: i32, w: i32, h: i32, flags: u32) -> *mut SDL_Window;
    pub fn SDL_DestroyWindow(window: *mut SDL_Window);

    pub fn SDL_SetWindowTitle(window: *mut SDL_Window, title: *const c_char);
}

pub type SDL_GLContext = *mut std::ffi::c_void;

extern {
    pub fn SDL_GL_CreateContext(window: *mut SDL_Window) -> SDL_GLContext;
    pub fn SDL_GL_DeleteContext(context: SDL_GLContext);
    pub fn SDL_GL_MakeCurrent(window: *mut SDL_Window, context: SDL_GLContext) -> i32;
    pub fn SDL_GL_GetProcAddress(proc: c_str) -> *mut std::ffi::c_void;
    pub fn SDL_GL_SetSwapInterval(interval: i32) -> i32;
    pub fn SDL_GL_SwapWindow(window: *mut SDL_Window);
}

#[repr(C)]
pub enum SDL_GLattr {
    RED_SIZE,
    GREEN_SIZE,
    BLUE_SIZE,
    ALPHA_SIZE,
    BUFFER_SIZE,
    DOUBLEBUFFER,
    DEPTH_SIZE,
    STENCIL_SIZE,
    ACCUM_RED_SIZE,
    ACCUM_GREEN_SIZE,
    ACCUM_BLUE_SIZE,
    ACCUM_ALPHA_SIZE,
    STEREO,
    MULTISAMPLEBUFFERS,
    MULTISAMPLESAMPLES,
    ACCELERATED_VISUAL,
    RETAINED_BACKING,
    CONTEXT_MAJOR_VERSION,
    CONTEXT_MINOR_VERSION,
    CONTEXT_EGL,
    CONTEXT_FLAGS,
    CONTEXT_PROFILE_MASK,
    SHARE_WITH_CURRENT_CONTEXT,
    FRAMEBUFFER_SRGB_CAPABLE,
    CONTEXT_RELEASE_BEHAVIOR,
    CONTEXT_RESET_NOTIFICATION,
    CONTEXT_NO_ERROR,
}

pub type SDL_GLprofile = i32;
pub const SDL_GL_CONTEXT_PROFILE_CORE: SDL_GLprofile           = 0x0001;
pub const SDL_GL_CONTEXT_PROFILE_COMPATIBILITY: SDL_GLprofile  = 0x0002;
pub const SDL_GL_CONTEXT_PROFILE_ES: SDL_GLprofile             = 0x0004; /**< GLX_CONTEXT_ES2_PROFILE_BIT_EXT */

pub type SDL_GLcontextFlag = i32;
pub const SDL_GL_CONTEXT_DEBUG_FLAG: SDL_GLcontextFlag = 0x0001;
pub const SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG: SDL_GLcontextFlag = 0x0002;
pub const SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG: SDL_GLcontextFlag = 0x0004;
pub const SDL_GL_CONTEXT_RESET_ISOLATION_FLAG: SDL_GLcontextFlag = 0x0008;

pub type SDL_GLContextResetNotification = i32;
pub const SDL_GL_RESET_NO_NOTIFICATION: SDL_GLContextResetNotification = 0x0000;
pub const SDL_GL_RESET_LOSE_CONTEXT: SDL_GLContextResetNotification = 0x0001;

extern {
    pub fn SDL_GL_SetAttribute(attr: SDL_GLattr, value: i32) -> i32;
    pub fn SDL_GL_GetAttribute(attr: SDL_GLattr, value: *mut i32) -> i32;
}
