use gl;
use log::{debug, error, info};
use sdl2_sys::sdl::video::{
    SDL_GLContext, SDL_GL_CreateContext, SDL_GL_DeleteContext, SDL_GL_GetProcAddress,
    SDL_GL_MakeCurrent, SDL_GL_SetSwapInterval, SDL_GL_SwapWindow, SDL_Window,
};
use sdl2_sys::sdl::error::SDL_GetError;

pub struct Context {
    context: SDL_GLContext,
    window: *mut SDL_Window,
}

impl Context {
    pub fn new() -> Self {
        Self {
            context: std::ptr::null_mut(),
            window: std::ptr::null_mut(),
        }
    }

    pub fn initialize(&mut self, win: *mut SDL_Window) -> bool {
        self.window = win;

        let ctx = unsafe { SDL_GL_CreateContext(win) };
        if ctx == std::ptr::null_mut() {
            error!("[SDL_GL_Context]: Failed to create OpenGL context");
            return false;
        }

        assert!(self.make_current());
        self.context = ctx;

        true
    }

    pub fn terminate(&self) {
        if self.context != std::ptr::null_mut() {
            unsafe {
                SDL_GL_DeleteContext(self.context);
            }
        }

        info!("[SDL_GL_Context]: Terminating EGL");
    }

    pub fn make_current(&self) -> bool {
        let success = unsafe { SDL_GL_MakeCurrent(self.window, self.context) };
        if success != 0 {
            let cstr = unsafe { std::ffi::CStr::from_ptr(SDL_GetError()) };
            error!(
                "[SDL_GL_Context]: Failed to make context current: {}",
                cstr.to_str().unwrap()
            );
            return false;
        } else {
            info!("[SDL_GL_Context]: Context made current");
            return true;
        }
    }

    pub fn swap_buffers(&self) {
        unsafe { SDL_GL_SwapWindow(self.window) }
    }

    pub fn set_swap_interval(&self, value: i32) {
        unsafe {
            SDL_GL_SetSwapInterval(value);
        }
    }

    pub fn load_gl() {
        let f = Self::get_proc_address("glCreateVertexArrays");
        info!("[SDL_GL_Context]: CreateVertexArrays: {:?}", f);
        gl::load_with(|s| Self::get_proc_address(s));
    }

    fn get_proc_address<T: AsRef<str>>(name: T) -> *const std::os::raw::c_void {
        let cstr = std::ffi::CString::new(name.as_ref()).unwrap();
        unsafe { SDL_GL_GetProcAddress(cstr.as_ptr()) }
    }

    pub fn setup_debug_callback(&self) {
        #[cfg(debug_assertions)]
        {
            unsafe {
                gl::Enable(gl::DEBUG_OUTPUT);
                gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
                gl::DebugMessageCallback(Some(log_gl_message), std::ptr::null());
            }
        }
    }
}

extern "system" fn log_gl_message(
    src: gl::types::GLenum,
    ty: gl::types::GLenum,
    id: gl::types::GLuint,
    severity: gl::types::GLenum,
    length: gl::types::GLsizei,
    msg: *const gl::types::GLchar,
    _user_param: *mut std::os::raw::c_void,
) {
    debug!(
        "[GLDebug]: Source: {}, Type: {}, Id: {}, Severity: {}, Length: {}",
        source_to_string(src),
        type_to_string(ty),
        id,
        severity_to_string(severity),
        length
    );
    if let Ok(msg) = unsafe { std::ffi::CStr::from_ptr(msg) }.to_str() {
        debug!("[GLDebug]: {}", msg);
    } else {
        debug!("[GLDebug]: Error message contains invalid UTF-8");
    }
}

fn source_to_string(src: gl::types::GLenum) -> &'static str {
    match src {
        gl::DEBUG_SOURCE_API => return "GL_DEBUG_SOURCE_API",
        gl::DEBUG_SOURCE_WINDOW_SYSTEM => return "GL_DEBUG_SOURCE_WINDOW_SYSTEM",
        gl::DEBUG_SOURCE_SHADER_COMPILER => return "GL_DEBUG_SOURCE_SHADER_COMPILER",
        gl::DEBUG_SOURCE_THIRD_PARTY => return "GL_DEBUG_SOURCE_THIRD_PARTY",
        gl::DEBUG_SOURCE_APPLICATION => return "GL_DEBUG_SOURCE_APPLICATION",
        gl::DEBUG_SOURCE_OTHER => return "GL_DEBUG_SOURCE_OTHER",
        _ => return "Unknown source",
    }
}

fn type_to_string(ty: gl::types::GLenum) -> &'static str {
    match ty {
        gl::DEBUG_TYPE_ERROR => return "GL_DEBUG_TYPE_ERROR",
        gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR",
        gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR",
        gl::DEBUG_TYPE_PORTABILITY => return "GL_DEBUG_TYPE_PORTABILITY",
        gl::DEBUG_TYPE_PERFORMANCE => return "GL_DEBUG_TYPE_PERFORMANCE",
        gl::DEBUG_TYPE_MARKER => return "GL_DEBUG_TYPE_MARKER",
        gl::DEBUG_TYPE_PUSH_GROUP => return "GL_DEBUG_TYPE_PUSH_GROUP",
        gl::DEBUG_TYPE_POP_GROUP => return "GL_DEBUG_TYPE_POP_GROUP",
        gl::DEBUG_TYPE_OTHER => return "GL_DEBUG_TYPE_OTHER",
        _ => return "Unknown type",
    }
}

fn severity_to_string(severity: gl::types::GLenum) -> &'static str {
    match severity {
        gl::DEBUG_SEVERITY_HIGH => return "GL_DEBUG_SEVERITY_HIGH",
        gl::DEBUG_SEVERITY_MEDIUM => return "GL_DEBUG_SEVERITY_MEDIUM",
        gl::DEBUG_SEVERITY_LOW => return "GL_DEBUG_SEVERITY_LOW",
        gl::DEBUG_SEVERITY_NOTIFICATION => return "GL_DEBUG_SEVERITY_NOTIFICATION",
        _ => return "Unknown severity",
    }
}
