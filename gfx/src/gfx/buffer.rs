use ::gl;

#[derive(Default)]
pub struct Buffer<'a> {
    handle: u32,
    size: u32,
    access: Access,
    is_triple_buffered: bool,
    pub mapped_data: Option<&'a mut [u8]>,
}

impl<'a> Buffer<'a> {
    pub fn create(size: u32, access: Access, is_triple_buffered: bool) -> Self {
        let mut handle: u32 = 0;
        let complete_size = if is_triple_buffered { size * 3 } else { size };
        unsafe {
            gl::CreateBuffers(1, &mut handle as _);
        }
        unsafe {
            gl::NamedBufferStorage(
                handle,
                complete_size as _,
                std::ptr::null(),
                access.get_gl_bits(),
            );
        }
        let data = match access {
            Access::None => None,
            Access::Read | Access::Write => {
                let data = unsafe {
                    gl::MapNamedBufferRange(handle, 0, complete_size as _, access.get_gl_bits())
                        as *mut u8
                };
                Some(unsafe { std::slice::from_raw_parts_mut(data, complete_size as _) })
            }
        };

        Self {
            handle,
            size,
            access,
            is_triple_buffered,
            mapped_data: data,
        }
    }

    pub fn destroy(&self) {
        if self.is_valid() {
            if self.mapped_data.is_some() {
                unsafe {
                    gl::UnmapNamedBuffer(self.handle);
                }
            }

            unsafe {
                gl::DeleteBuffers(1, &self.handle);
            }
        }
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0
    }

    pub fn size(&self) -> u32 {
        self.size
    }

    pub fn complete_size(&self) -> u32 {
        if self.is_triple_buffered {
            self.size() * 3
        } else {
            self.size()
        }
    }

    pub fn get_handle(&self) -> u32 {
        self.handle
    }
}

pub enum Access {
    None,
    Read,
    Write,
}

impl Default for Access {
    fn default() -> Self {
        Self::None
    }
}

impl Access {
    fn get_gl_bits(&self) -> gl::types::GLenum {
        match self {
            Self::None => 0,
            Self::Read => gl::MAP_READ_BIT | gl::MAP_PERSISTENT_BIT | gl::MAP_COHERENT_BIT,
            Self::Write => gl::MAP_WRITE_BIT | gl::MAP_PERSISTENT_BIT | gl::MAP_COHERENT_BIT,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct BufferView {
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

impl BufferView {
    pub fn new() -> Self {
        Self {
            handle: 0,
            offset: 0,
            size: 0,
        }
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0 && self.size != 0
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct BufferSpace {
    pub offset: u32,
    pub size: u32,
}

impl BufferSpace {
    pub fn is_valid(&self) -> bool {
        self.size != 0
    }
}
