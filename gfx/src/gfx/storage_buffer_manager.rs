use super::buffer::{Access, Buffer};
use gfx_common::config::{MAX_STORAGE_BUFFERS};
use gfx_common::StorageBufferHandle;
use log::error;

pub struct StorageBufferManager<'a> {
    storage_buffers: [StorageBuffer<'a>; MAX_STORAGE_BUFFERS],
}

impl<'a> StorageBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            storage_buffers: {
                let mut arr: [StorageBuffer<'a>; MAX_STORAGE_BUFFERS] =
                    unsafe { std::mem::MaybeUninit::uninit().assume_init() };
                for item in arr.iter_mut() {
                    unsafe {
                        std::ptr::write(item, StorageBuffer::default());
                    }
                }
                arr
            },
        }
    }

    pub fn create_storage_buffer(&mut self, size: u32) -> StorageBufferHandle {
        let size = size + (16 - size % 16); // align to 16 bytes

        for (i, ub) in (0u16..).zip(self.storage_buffers.iter_mut()) {
            if ub.buffer.is_valid() {
                continue;
            }

            ub.buffer = Buffer::create(size, Access::None, false);

            return StorageBufferHandle::new(i);
        }

        error!("[StorageBufferManager]: Reached storage buffer limit. Can't create a new one");
        StorageBufferHandle::invalid()
    }

    pub fn destroy_storage_buffer(&mut self, handle: StorageBufferHandle) {
        if handle.get_id() > MAX_STORAGE_BUFFERS as _ {
            error!("[StorageBufferManager]: Destroy storage buffer failed: storage buffer index out of bounds");
            return;
        }

        self.storage_buffers[handle.get_id() as usize].buffer.destroy();
        self.storage_buffers[handle.get_id() as usize] = StorageBuffer::default();
    }

    pub fn get_storage_buffer(&self, handle: StorageBufferHandle) -> Option<&StorageBuffer> {
        if handle.get_id() > MAX_STORAGE_BUFFERS as _ {
            error!("[StorageBufferManager]: Get storage buffer failed: storage buffer index out of bounds");
            return None;
        }

        Some(&self.storage_buffers[handle.get_id() as usize])
    }
}

#[derive(Default)]
pub struct StorageBuffer<'a> {
    pub buffer: Buffer<'a>,
}
