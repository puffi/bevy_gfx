use gfx_common::{Attachment, ClearValue, RenderTargetHandle};
use ::gl;
use gfx_common::config::MAX_RENDER_TARGETS;
use crate::gfx::texture_manager::Texture;
use crate::gfx::utils::attachment_gl_type;
use log::error;

use std::collections::HashMap;

pub struct RenderTargetManager {
    render_targets: [RenderTarget; MAX_RENDER_TARGETS],
}

impl RenderTargetManager {
    pub fn new() -> Self {
        let mut arr: [RenderTarget; MAX_RENDER_TARGETS] =
            unsafe { std::mem::MaybeUninit::uninit().assume_init() };
        for item in arr.iter_mut() {
            unsafe {
                std::ptr::write(item, RenderTarget::new());
            }
        }
        arr[0].handle = 0;
        let color = ClearValue::Vec4([0f32, 0f32, 0f32, 1f32]);
        arr[0].clear_values.insert(Attachment::Color0, color);
        let depth_stencil = ClearValue::F32I32(1f32, 0);
        arr[0]
            .clear_values
            .insert(Attachment::DepthStencil, depth_stencil);
        arr[0].is_initialized = true;
        Self {
            render_targets: arr,
        }
    }

    pub fn create_render_target(
        &mut self,
        textures: HashMap<Attachment, &Texture>,
        clear_values: HashMap<Attachment, ClearValue>,
    ) -> RenderTargetHandle {
        for (i, rt) in (0u16..).zip(self.render_targets.iter_mut()) {
            if !rt.is_initialized {
                continue;
            }

            rt.clear_values = clear_values;

            let mut draw_buffers = Vec::new();
            unsafe {
                gl::CreateFramebuffers(1, &mut rt.handle as _);
            }
            for (att, tex) in textures.iter() {
                unsafe {
                    gl::NamedFramebufferTexture(
                        rt.handle,
                        attachment_gl_type(att),
                        (**tex).handle(),
                        0,
                    );
                }
                match att {
                    Attachment::Color0 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color1 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color2 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color3 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color4 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color5 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color6 => draw_buffers.push(attachment_gl_type(att)),
                    Attachment::Color7 => draw_buffers.push(attachment_gl_type(att)),
                    _ => {}
                }
            }
            unsafe {
                gl::NamedFramebufferDrawBuffers(
                    rt.handle,
                    draw_buffers.len() as _,
                    draw_buffers.as_ptr(),
                );
            }

            rt.is_initialized = true;

            return RenderTargetHandle::new(i);
        }

        error!("[RenderTargetManager]: No free render target slot available");
        RenderTargetHandle::invalid()
    }

    pub fn destroy_render_target(&mut self, handle: RenderTargetHandle) {
        if handle.get_id() >= MAX_RENDER_TARGETS as _ {
            error!("[RenderTargetManager]: Destroy render target failed: render target index out of range");
            return;
        }

        unsafe {
            gl::DeleteFramebuffers(1, &mut self.render_targets[handle.get_id() as usize].handle);
        }
        self.render_targets[handle.get_id() as usize] = RenderTarget::new();
    }

    pub fn get_render_target(
        &self,
        handle: RenderTargetHandle,
    ) -> Result<&RenderTarget, &'static str> {
        if handle.get_id() >= MAX_RENDER_TARGETS as _ {
            error!(
                "[RenderTargetManager]: Get render target failed: render target index out of range"
            );
            return Err(
                "[RenderTargetManager]: Get render target failed: render target index out of range",
            );
        }

        if !self.render_targets[handle.get_id() as usize].is_initialized {
            error!("[RenderTargetManager]: Get render target failed: render target {} is not initialized", handle.get_id());
            return Err("[RenderTargetManager]: Get render target failed: render target {} is not initialized");
        }

        Ok(&self.render_targets[handle.get_id() as usize])
    }

    pub fn clear_render_target(&self, handle: RenderTargetHandle) {
        let rt = match self.get_render_target(handle) {
            Ok(rt) => rt,
            Err(e) => {
                error!("[RenderTargetManager]: Can't clear render target: {}", e);
                return;
            }
        };
        for (att, cv) in rt.clear_values.iter() {
            if *cv == ClearValue::None {
                continue;
            }
            self.clear_render_target_handle_attachment_clearvalue(rt.handle, att, cv);
        }
    }

    pub fn clear_render_target_attachment_clearvalue(
        &self,
        handle: RenderTargetHandle,
        att: &Attachment,
        cv: &ClearValue,
    ) {
        let rt = match self.get_render_target(handle) {
            Ok(rt) => rt,
            Err(e) => {
                error!("[RenderTargetManager]: Can't clear render target: {}", e);
                return;
            }
        };
        self.clear_render_target_handle_attachment_clearvalue(rt.handle, att, cv);
    }

    fn clear_render_target_handle_attachment_clearvalue(
        &self,
        handle: u32,
        att: &Attachment,
        cv: &ClearValue,
    ) {
        match att {
            Attachment::Color0 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color1 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color2 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color3 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color4 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color5 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color6 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Color7 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Depth => {
                if let ClearValue::F32(depth) = cv {
                    Self::clear_render_target_depth(handle, *depth);
                } else {
                    error!("[RenderTargetManager]: Attachment::Depth requires a clear value of type 'F32'");
                }
            }
            Attachment::Stencil => {
                if let ClearValue::I32(stencil) = cv {
                    Self::clear_render_target_stencil(handle, *stencil);
                } else {
                    error!("[RenderTargetManager]: Attachment::Stencil requires a clear value of type 'I32'");
                }
            }
            Attachment::DepthStencil => {
                if let ClearValue::F32I32(depth, stencil) = cv {
                    Self::clear_render_target_depth_stencil(handle, *depth, *stencil);
                } else {
                    error!("[RenderTargetManager]: Attachment::DepthStencil requires a clear value of type 'F32I32'");
                }
            }
        }
    }

    fn clear_render_target_color(handle: u32, att: &Attachment, cv: &ClearValue) {
        match cv {
            ClearValue::None => return,
            ClearValue::F32(val) => unsafe {
                gl::ClearNamedFramebufferfv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    &*val as _,
                )
            },
            ClearValue::I32(val) => unsafe {
                gl::ClearNamedFramebufferiv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    &*val as _,
                )
            },
            ClearValue::U32(val) => unsafe {
                gl::ClearNamedFramebufferuiv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    &*val as _,
                )
            },
            ClearValue::Vec4(val) => unsafe {
                gl::ClearNamedFramebufferfv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    &*val as _,
                )
            },
            ClearValue::F32I32(_, _) => {
                error!("[RenderTargetManager]: Wrong clear value type 'F32I32' for color texture");
                assert!(false);
            }
        }
    }

    fn clear_render_target_depth(handle: u32, depth: f32) {
        unsafe {
            gl::ClearNamedFramebufferfv(handle, gl::DEPTH, 0, &depth);
        }
    }

    fn clear_render_target_stencil(handle: u32, stencil: i32) {
        unsafe {
            gl::ClearNamedFramebufferiv(handle, gl::STENCIL, 0, &stencil);
        }
    }

    fn clear_render_target_depth_stencil(handle: u32, depth: f32, stencil: i32) {
        unsafe {
            gl::ClearNamedFramebufferfi(handle, gl::DEPTH_STENCIL, 0, depth, stencil);
        }
    }
}

pub struct RenderTarget {
    clear_values: HashMap<Attachment, ClearValue>,
    handle: u32,
    is_initialized: bool,
}

impl RenderTarget {
    pub fn new() -> Self {
        Self {
            clear_values: HashMap::new(),
            handle: 0,
            is_initialized: false,
        }
    }

    pub fn get_handle(&self) -> u32 {
        self.handle
    }
}

fn color_attachment_to_index(att: &Attachment) -> i32 {
    match att {
        Attachment::Color0 => 0,
        Attachment::Color1 => 1,
        Attachment::Color2 => 2,
        Attachment::Color3 => 3,
        Attachment::Color4 => 4,
        Attachment::Color5 => 5,
        Attachment::Color6 => 6,
        Attachment::Color7 => 7,
        _ => std::i32::MAX,
    }
}
