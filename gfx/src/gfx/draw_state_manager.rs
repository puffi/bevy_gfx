use ::gl;

use gfx_common::{
    AttributeType, BlendEquation, BlendFactor, BlendFunc, BlendState, CullFrontFace, CullMode,
    CullState, DepthFunc, DepthState, IndexType, ScissorState, TextureType, Viewport,
};
use gfx_common::config::{MAX_BOUND_STORAGE_BUFFERS, MAX_BOUND_TEXTURES, MAX_BOUND_UNIFORM_BUFFERS};
use super::rt_manager::RenderTarget;
use super::shader_manager::Shader;
use super::vertex_buffer_manager::{IndexBuffer, VertexBuffer};

use log::debug;

pub struct DrawStateManager {
    vao: u32,
    program_pipeline: u32,
    draw_state: DrawState,
}

impl DrawStateManager {
    pub fn new() -> Self {
        let vao = 0;
        let program_pipeline = 0;
        Self {
            vao,
            program_pipeline,
            draw_state: DrawState::new(),
        }
    }

    pub fn initialize(&mut self) -> bool {
        unsafe {
            gl::CreateVertexArrays(1, &mut self.vao);
        }
        unsafe {
            gl::BindVertexArray(self.vao);
        }
        unsafe {
            gl::CreateProgramPipelines(1, &mut self.program_pipeline);
        }
        unsafe {
            gl::BindProgramPipeline(self.program_pipeline);
        }
        true
    }

    pub fn destroy(&self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
        }
        unsafe {
            gl::DeleteProgramPipelines(1, &self.program_pipeline);
        }
    }

    pub fn set_vertex_buffer(&mut self, vertex_buffer: Option<&VertexBuffer>) {
        if let Some(vertex_buffer) = vertex_buffer {
            if self.draw_state.vertex_buffer == vertex_buffer.view.handle
                && self.draw_state.vertex_count == vertex_buffer.count
                && self.draw_state.base_vertex == vertex_buffer.base_vertex as _
            {
                return;
            }
            debug!(
                "[DrawStateManager]: Binding vertex buffer: {}",
                vertex_buffer.view.handle
            );

            for (i, attr) in (0u32..).zip(vertex_buffer.attributes.iter()) {
                if attr.ty == AttributeType::None {
                    continue;
                }
                unsafe {
                    gl::EnableVertexArrayAttrib(self.vao, attr.location as _);
                }
                unsafe {
                    gl::VertexArrayAttribFormat(
                        self.vao,
                        attr.location as _,
                        attr.num as _,
                        attr.ty.to_gl(),
                        attr.normalize as _,
                        vertex_buffer.offsets[i as usize],
                    );
                }
                unsafe {
                    gl::VertexArrayAttribBinding(self.vao, attr.location as _, 0);
                }
            }

            unsafe {
                gl::VertexArrayVertexBuffer(
                    self.vao,
                    0,
                    vertex_buffer.view.handle,
                    0,
                    vertex_buffer.vertex_size as _,
                );
            }

            self.draw_state.vertex_buffer = vertex_buffer.view.handle;
            self.draw_state.base_vertex = vertex_buffer.base_vertex as _;
            self.draw_state.vertex_count = vertex_buffer.count;
        }
    }

    pub fn set_index_buffer(&mut self, index_buffer: Option<&IndexBuffer>) {
        if let Some(index_buffer) = index_buffer {
            if self.draw_state.index_buffer == index_buffer.view.handle
                && self.draw_state.index_count == index_buffer.count as _
                && self.draw_state.index_offset == index_buffer.view.offset
                && self.draw_state.index_type == index_buffer.ty
            {
                return;
            }
            debug!(
                "[DrawStateManager]: Binding index buffer: {}",
                index_buffer.view.handle
            );

            unsafe {
                gl::VertexArrayElementBuffer(self.vao, index_buffer.view.handle);
            }

            self.draw_state.index_buffer = index_buffer.view.handle;
            self.draw_state.index_count = index_buffer.count as _;
            self.draw_state.index_offset = index_buffer.view.offset;
            self.draw_state.index_type = index_buffer.ty;
        } else {
            if self.draw_state.index_buffer == 0 {
                return;
            }

            debug!(
                "[DrawStateManager]: Unbinding index buffer: {}",
                self.draw_state.index_buffer
            );
            unsafe {
                gl::VertexArrayElementBuffer(self.vao, 0);
            }

            self.draw_state.index_buffer = 0;
            self.draw_state.index_count = 0;
            self.draw_state.index_offset = 0;
        }
    }

    pub fn set_vertex_shader(&mut self, shader: Option<&Shader>) {
        if let Some(shader) = shader {
            if self.draw_state.vertex_shader == shader.get_handle() {
                return;
            }

            debug!(
                "[DrawStateManager]: Binding vertex shader: {}",
                shader.get_handle()
            );
            unsafe {
                gl::UseProgramStages(
                    self.program_pipeline,
                    gl::VERTEX_SHADER_BIT,
                    shader.get_handle(),
                );
            }
            self.draw_state.vertex_shader = shader.get_handle();
        } else {
            if self.draw_state.vertex_shader == 0 {
                return;
            }

            debug!(
                "[DrawStateManager]: Unbinding vertex shader: {}",
                self.draw_state.vertex_shader
            );
            unsafe {
                gl::UseProgramStages(self.program_pipeline, gl::VERTEX_SHADER_BIT, 0);
            }
            self.draw_state.vertex_shader = 0;
        }
    }

    pub fn set_fragment_shader(&mut self, shader: Option<&Shader>) {
        if let Some(shader) = shader {
            if self.draw_state.fragment_shader == shader.get_handle() {
                return;
            }

            debug!(
                "[DrawStateManager]: Binding fragment shader: {}",
                shader.get_handle()
            );
            unsafe {
                gl::UseProgramStages(
                    self.program_pipeline,
                    gl::FRAGMENT_SHADER_BIT,
                    shader.get_handle(),
                );
            }
            self.draw_state.fragment_shader = shader.get_handle();
        } else {
            if self.draw_state.fragment_shader == 0 {
                return;
            }

            debug!(
                "[DrawStateManager]: Unbinding fragment shader: {}",
                self.draw_state.fragment_shader
            );
            unsafe {
                gl::UseProgramStages(self.program_pipeline, gl::FRAGMENT_SHADER_BIT, 0);
            }
            self.draw_state.fragment_shader = 0;
        }
    }

    pub fn set_blend_state(&mut self, blend_state: BlendState) {
        if self.draw_state.blend_state != blend_state {
            if self.draw_state.blend_state.is_enabled != blend_state.is_enabled {
                if blend_state.is_enabled {
                    debug!("[DrawStateManager]: Enabling blending");
                    unsafe {
                        gl::Enable(gl::BLEND);
                    }
                } else {
                    debug!("[DrawStateManager]: Disabling blending");
                    unsafe {
                        gl::Disable(gl::BLEND);
                    }
                }
            }

            if self.draw_state.blend_state.func != blend_state.func {
                debug!(
                    "[DrawStateManager]: Setting blend function to: {:?}",
                    blend_state.func
                );
                unsafe {
                    gl::BlendFunc(
                        blend_state.func.source.to_gl(),
                        blend_state.func.destination.to_gl(),
                    );
                }
            }

            if self.draw_state.blend_state.equation != blend_state.equation {
                debug!(
                    "[DrawStateManager]: Setting blend equation to: {:?}",
                    blend_state.equation
                );
                unsafe {
                    gl::BlendEquation(blend_state.equation.to_gl());
                }
            }

            self.draw_state.blend_state = blend_state;
        }
    }

    pub fn set_depth_state(&mut self, depth_state: DepthState) {
        if self.draw_state.depth_state != depth_state {
            if self.draw_state.depth_state.is_enabled != depth_state.is_enabled {
                if depth_state.is_enabled {
                    debug!("[DrawStateManager]: Enabling depth test");
                    unsafe {
                        gl::Enable(gl::DEPTH_TEST);
                    }
                } else {
                    debug!("[DrawStateManager]: Disabling depth test");
                    unsafe {
                        gl::Disable(gl::DEPTH_TEST);
                    }
                }
            }

            if self.draw_state.depth_state.func != depth_state.func {
                debug!(
                    "[DrawStateManager]: Setting depth function to: {:?}",
                    depth_state.func
                );
                unsafe {
                    gl::DepthFunc(depth_state.func.to_gl());
                }
            }

            self.draw_state.depth_state = depth_state;
        }
    }

    pub fn set_cull_state(&mut self, cull_state: CullState) {
        if self.draw_state.cull_state != cull_state {
            if self.draw_state.cull_state.is_enabled != cull_state.is_enabled {
                if cull_state.is_enabled {
                    debug!("[DrawStateManager]: Enable culling");
                    unsafe {
                        gl::Enable(gl::CULL_FACE);
                    }
                } else {
                    debug!("[DrawStateManager]: Disable culling");
                    unsafe {
                        gl::Disable(gl::CULL_FACE);
                    }
                }
            }

            if self.draw_state.cull_state.mode != cull_state.mode {
                debug!(
                    "[DrawStateManager]: Setting culling mode to: {:?}",
                    cull_state.mode
                );
                unsafe {
                    gl::CullFace(cull_state.mode.to_gl());
                }
            }

            if self.draw_state.cull_state.front_face != cull_state.front_face {
                debug!(
                    "[DrawStageManager]: Setting culling front face to: {:?}",
                    cull_state.front_face
                );

                unsafe {
                    gl::FrontFace(cull_state.front_face.to_gl());
                }
            }

            self.draw_state.cull_state = cull_state;
        }
    }

    pub fn set_viewport(&mut self, vp: Viewport) {
        if self.draw_state.viewport != vp {
            debug!(
                "[DrawStateManager]: Setting viewport to ({},{},{},{})",
                vp.x, vp.y, vp.width, vp.height
            );
            unsafe {
                gl::Viewport(vp.x, vp.y, vp.width, vp.height);
            }

            self.draw_state.viewport = vp;
        }
    }

    pub fn set_uniform_buffer(&mut self, binding: UniformBinding) {
        if self.draw_state.uniform_bindings[binding.location as usize] != binding {
            if binding.handle == 0 {
                debug!(
                    "[DrawStateManager]: Unbinding uniform buffer at location: {}",
                    binding.location
                );
            } else {
                debug!("[DrawStateManager]: Binding buffer: {} to uniform buffer location: {} with offset: {} and size: {}", binding.handle, binding.location, binding.offset, binding.size);
            }

            unsafe {
                gl::BindBufferRange(
                    gl::UNIFORM_BUFFER,
                    binding.location,
                    binding.handle,
                    binding.offset as _,
                    binding.size as _,
                );
            }

            self.draw_state.uniform_bindings[binding.location as usize] = binding;
        }
    }

    pub fn set_storage_buffer(&mut self, binding: StorageBinding) {
        if self.draw_state.storage_bindings[binding.location as usize] != binding {
            if binding.handle == 0 {
                debug!(
                    "[DrawStateManager]: Unbinding storage buffer at location: {}",
                    binding.location
                );
            } else {
                debug!("[DrawStateManager]: Binding buffer: {} to storage buffer location: {} with offset: {} and size: {}", binding.handle, binding.location, binding.offset, binding.size);
            }

            unsafe {
                gl::BindBufferRange(
                    gl::SHADER_STORAGE_BUFFER,
                    binding.location,
                    binding.handle,
                    binding.offset as _,
                    binding.size as _,
                );
            }

            self.draw_state.storage_bindings[binding.location as usize] = binding;
        }
    }

    pub fn set_texture(&mut self, binding: TextureBinding) {
        if self.draw_state.texture_bindings[binding.location as usize] != binding {
            if binding.handle == 0 {
                debug!(
                    "[DrawStateManager]: Unbinding texture at location: {}",
                    binding.location
                );
            } else {
                debug!("[DrawStateManager]: Binding texture: {} of type: {} and sampler: {} to location: {}", binding.handle, binding.ty, binding.sampler, binding.location);
            }
            unsafe {
                gl::ActiveTexture(gl::TEXTURE0 + binding.location);
            }
            unsafe {
                gl::BindTexture(binding.ty.to_gl(), binding.handle);
            }
            unsafe {
                gl::BindSampler(binding.location, binding.sampler);
            }

            self.draw_state.texture_bindings[binding.location as usize] = binding;
        }
    }

    pub fn set_render_targets(
        &mut self,
        read: Option<&RenderTarget>,
        write: Option<&RenderTarget>,
    ) {
        if let Some(read) = read {
            if read.get_handle() != self.draw_state.read_target {
                debug!(
                    "[DrawStateManager]: Binding read target: {}",
                    read.get_handle()
                );
                self.draw_state.read_target = read.get_handle();
                unsafe {
                    gl::BindFramebuffer(gl::READ_FRAMEBUFFER, read.get_handle());
                }
            }
        } else {
            if self.draw_state.read_target != 0 {
                debug!("[DrawStateManager]: Binding default read target");
                self.draw_state.read_target = 0;
                unsafe {
                    gl::BindFramebuffer(gl::READ_FRAMEBUFFER, 0);
                }
            }
        }

        if let Some(write) = write {
            if write.get_handle() != self.draw_state.write_target {
                debug!(
                    "[DrawStateManager]: Binding write target: {}",
                    write.get_handle()
                );
                self.draw_state.write_target = write.get_handle();
                unsafe {
                    gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, write.get_handle());
                }
            }
        } else {
            if self.draw_state.write_target != 0 {
                debug!("[DrawStateManager]: Binding default write target");
                self.draw_state.write_target = 0;
                unsafe {
                    gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, 0);
                }
            }
        }
    }

    pub fn draw(&self, instances: i32, base_instance: u32) {
        if self.draw_state.index_buffer != 0 {
            unsafe {
                gl::DrawElementsInstancedBaseVertexBaseInstance(
                    gl::TRIANGLES,
                    self.draw_state.index_count,
                    self.draw_state.index_type.to_gl(),
                    self.draw_state.index_offset as _,
                    instances,
                    self.draw_state.base_vertex,
                    base_instance,
                );
            }
        } else {
            unsafe {
                gl::DrawArraysInstancedBaseInstance(
                    gl::TRIANGLES,
                    self.draw_state.base_vertex,
                    self.draw_state.vertex_count as _,
                    instances,
                    base_instance,
                );
            }
        }
    }
}

struct DrawState {
    vertex_buffer: u32,
    index_buffer: u32,

    base_vertex: i32,
    vertex_count: u32,
    index_count: i32,
    index_offset: u32,
    index_type: IndexType,

    vertex_shader: u32,
    fragment_shader: u32,

    blend_state: BlendState,
    depth_state: DepthState,
    cull_state: CullState,
    scissor_state: ScissorState,
    viewport: Viewport,

    uniform_bindings: [UniformBinding; MAX_BOUND_UNIFORM_BUFFERS],
    storage_bindings: [StorageBinding; MAX_BOUND_STORAGE_BUFFERS],
    texture_bindings: [TextureBinding; MAX_BOUND_TEXTURES],

    read_target: u32,
    write_target: u32,
}

impl DrawState {
    fn new() -> Self {
        Self {
            vertex_buffer: 0,
            index_buffer: 0,
            base_vertex: 0,
            vertex_count: 0,
            index_count: 0,
            index_offset: 0,
            index_type: IndexType::U32,
            vertex_shader: 0,
            fragment_shader: 0,
            blend_state: BlendState {
                is_enabled: false,
                func: BlendFunc::default(),
                equation: BlendEquation::Add,
            },
            depth_state: DepthState {
                is_enabled: false,
                func: DepthFunc::Less,
            },
            cull_state: CullState {
                is_enabled: false,
                mode: CullMode::Back,
                front_face: CullFrontFace::Ccw,
            },
            scissor_state: ScissorState {
                is_enabled: false,
                rect: [0; 4],
            },
            viewport: Viewport {
                x: 0,
                y: 0,
                width: 0,
                height: 0,
            },

            uniform_bindings: [UniformBinding {
                location: 0,
                handle: 0,
                offset: 0,
                size: 0,
            }; MAX_BOUND_UNIFORM_BUFFERS],
            storage_bindings: [StorageBinding {
                location: 0,
                handle: 0,
                offset: 0,
                size: 0,
            }; MAX_BOUND_STORAGE_BUFFERS],
            texture_bindings: [TextureBinding {
                handle: 0,
                sampler: 0,
                location: 0,
                ty: TextureType::Single1D,
            }; MAX_BOUND_TEXTURES],

            read_target: 0,
            write_target: 0,
        }
    }
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct UniformBinding {
    pub location: u32,
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct StorageBinding {
    pub location: u32,
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct TextureBinding {
    pub handle: u32,
    pub sampler: u32,
    pub location: u32,
    pub ty: TextureType,
}

trait ToGlType {
    fn to_gl(&self) -> gl::types::GLenum;
}

impl ToGlType for IndexType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::U16 => gl::UNSIGNED_SHORT,
            Self::U32 => gl::UNSIGNED_INT,
        }
    }
}

impl ToGlType for TextureType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Single1D => gl::TEXTURE_1D,
            Self::Single2D => gl::TEXTURE_2D,
            Self::Single3D => gl::TEXTURE_3D,
            Self::Array1D => gl::TEXTURE_1D_ARRAY,
            Self::Array2D => gl::TEXTURE_2D_ARRAY,
        }
    }
}

impl ToGlType for CullMode {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Front => gl::FRONT,
            Self::Back => gl::BACK,
            Self::FrontAndBack => gl::FRONT_AND_BACK,
        }
    }
}

impl ToGlType for CullFrontFace {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Cw => gl::CW,
            Self::Ccw => gl::CCW,
        }
    }
}

impl ToGlType for DepthFunc {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Never => gl::NEVER,
            Self::Less => gl::LESS,
            Self::Equal => gl::EQUAL,
            Self::LEqual => gl::LEQUAL,
            Self::Greater => gl::GREATER,
            Self::NotEqual => gl::NOTEQUAL,
            Self::GEqual => gl::GEQUAL,
            Self::Always => gl::ALWAYS,
        }
    }
}

impl ToGlType for BlendFactor {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Zero => gl::ZERO,
            Self::One => gl::ONE,
            Self::SrcColor => gl::SRC_COLOR,
            Self::OneMinusSrcColor => gl::ONE_MINUS_SRC_COLOR,
            Self::DstColor => gl::DST_COLOR,
            Self::OneMinusDstColor => gl::ONE_MINUS_DST_COLOR,
            Self::SrcAlpha => gl::SRC_ALPHA,
            Self::OneMinusSrcAlpha => gl::ONE_MINUS_SRC_ALPHA,
            Self::DstAlpha => gl::DST_ALPHA,
            Self::OneMinusDstAlpha => gl::ONE_MINUS_DST_ALPHA,
            Self::ConstantColor => gl::CONSTANT_COLOR,
            Self::OneMinusConstantColor => gl::ONE_MINUS_CONSTANT_COLOR,
            Self::ConstantAlpha => gl::CONSTANT_ALPHA,
            Self::OneMinusConstantAlpha => gl::ONE_MINUS_CONSTANT_ALPHA,
            Self::SrcAlphaSaturate => gl::SRC_ALPHA_SATURATE,
            Self::Src1Color => gl::SRC1_COLOR,
            Self::OneMinusSrc1Color => gl::ONE_MINUS_SRC1_COLOR,
            // 	src1Alpha => gl::SRC1_ALPHA,
            Self::OneMinusSrc1Alpha => gl::ONE_MINUS_SRC1_ALPHA,
        }
    }
}

impl ToGlType for BlendEquation {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Add => gl::FUNC_ADD,
            Self::Subtract => gl::FUNC_SUBTRACT,
            Self::ReverseSubtract => gl::FUNC_REVERSE_SUBTRACT,
            Self::Min => gl::MIN,
            Self::Max => gl::MAX,
        }
    }
}

impl ToGlType for AttributeType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::None => 0,
            Self::U8 => gl::UNSIGNED_BYTE,
            Self::F32 => gl::FLOAT,
        }
    }
}
