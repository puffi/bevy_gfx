use super::buffer::{Access, Buffer, BufferSpace, BufferView};
use gfx_common::config::{GPU_BUFFER_SIZE, MAX_ATTRIBUTES, MAX_INDEX_BUFFERS, MAX_VERTEX_BUFFERS};
use gfx_common::{Attribute, AttributeType, IndexBufferHandle, IndexType, VertexBufferHandle};
use log::{debug, error};

pub struct VertexBufferManager<'a> {
    vertex_buffers: [VertexBuffer; MAX_VERTEX_BUFFERS],
    index_buffers: [IndexBuffer; MAX_INDEX_BUFFERS],

    gpu_buffers: Vec<GPUBuffer<'a>>,
}

impl<'a> VertexBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            vertex_buffers: [VertexBuffer::new(); MAX_VERTEX_BUFFERS],
            index_buffers: [IndexBuffer::new(); MAX_INDEX_BUFFERS],
            gpu_buffers: Vec::new(),
        }
    }

    pub fn create_vertex_buffer(
        &mut self,
        data: &[u8],
        attributes: &[Attribute],
    ) -> VertexBufferHandle {
        for (i, vb) in (0u16..).zip(self.vertex_buffers.iter()) {
            if vb.view.is_valid() {
                continue;
            }

            return self.create_vertex_buffer_index(i, data, attributes);
        }

        error!("[VertexBufferManager]: Reached vertex buffer limit. Can't create a new one");
        VertexBufferHandle::invalid()
    }

    fn create_vertex_buffer_index(
        &mut self,
        index: u16,
        data: &[u8],
        attributes: &[Attribute],
    ) -> VertexBufferHandle {
        if data.len() > GPU_BUFFER_SIZE as _ {
            error!(
                "[VertexBufferManager]: Cannot allocate vertex buffer with size > {}",
                GPU_BUFFER_SIZE
            );
            return VertexBufferHandle::invalid();
        }

        for buffer in self.gpu_buffers.iter_mut() {
            let space = buffer.allocate(data.len() as u32, vertex_size(attributes));
            if !space.is_valid() {
                continue;
            }

            let mut vb = &mut self.vertex_buffers[index as usize];
            vb.view = BufferView {
                handle: buffer.buffer.get_handle(),
                offset: space.offset,
                size: space.size,
            };
            vb.attributes[..attributes.len()].clone_from_slice(attributes);
            calculate_offsets(&mut vb.offsets, attributes);
            vb.vertex_size = vertex_size(attributes);
            vb.count = (data.len() / vb.vertex_size as usize) as _;
            vb.base_vertex = vb.view.offset / vb.vertex_size;

            debug!("[VertexBufferManager]: Allocated vertex buffer: {:?}", *vb);
            return VertexBufferHandle::new(index);
        }

        self.gpu_buffers.push(GPUBuffer::new());
        return self.create_vertex_buffer_index(index, data, attributes);
    }

    pub fn destroy_vertex_buffer(&mut self, handle: VertexBufferHandle) {
        if handle.get_id() > MAX_VERTEX_BUFFERS as _ {
            error!("[VertexBufferManager]: Destroy vertex buffer failed: vertex buffer index out of bounds");
            return;
        }

        let view = self.vertex_buffers[handle.get_id() as usize].view;
        self.vertex_buffers[handle.get_id() as usize] = VertexBuffer::new();
        for buffer in self.gpu_buffers.iter_mut() {
            if buffer.buffer.get_handle() == view.handle {
                buffer.free(BufferSpace {
                    offset: view.offset,
                    size: view.size,
                });
            }
        }
    }

    pub fn get_vertex_buffer(&mut self, handle: VertexBufferHandle) -> Option<&VertexBuffer> {
        if handle.get_id() > MAX_VERTEX_BUFFERS as _ {
            error!("[VertexBufferManager]: Get vertex buffer failed: vertex buffer index out of bounds");
            return None;
        }

        Some(&self.vertex_buffers[handle.get_id() as usize])
    }

    pub fn create_index_buffer(&mut self, data: &[u8], ty: IndexType) -> IndexBufferHandle {
        for (i, ib) in (0u16..).zip(self.index_buffers.iter()) {
            if ib.view.is_valid() {
                continue;
            }

            return self.create_index_buffer_index(i, data, ty);
        }

        error!("[VertexBufferManager]: Reached index buffer limit. Can't create a new one");
        IndexBufferHandle::invalid()
    }

    fn create_index_buffer_index(
        &mut self,
        index: u16,
        data: &[u8],
        ty: IndexType,
    ) -> IndexBufferHandle {
        if data.len() > GPU_BUFFER_SIZE as _ {
            error!(
                "[VertexBufferManager]: Cannot allocate index buffer with size > {}",
                GPU_BUFFER_SIZE
            );
            return IndexBufferHandle::invalid();
        }

        for buffer in self.gpu_buffers.iter_mut() {
            let space = buffer.allocate(data.len() as u32, ty.get_size());
            if !space.is_valid() {
                continue;
            }

            let mut ib = &mut self.index_buffers[index as usize];

            ib.view = BufferView {
                handle: buffer.buffer.get_handle(),
                offset: space.offset,
                size: space.size,
            };
            ib.ty = ty;
            ib.count = (data.len() / ty.get_size() as usize) as u32;

            debug!("[VertexBufferManager]: Allocated index buffer: {:?}", *ib);
            return IndexBufferHandle::new(index);
        }

        self.gpu_buffers.push(GPUBuffer::new());
        return self.create_index_buffer_index(index, data, ty);
    }

    pub fn destroy_index_buffer(&mut self, handle: IndexBufferHandle) {
        if handle.get_id() > MAX_INDEX_BUFFERS as _ {
            error!("[VertexBufferManager]: Destroy index buffer failed: index buffer index out of bounds");
            return;
        }

        let view = self.index_buffers[handle.get_id() as usize].view;
        self.index_buffers[handle.get_id() as usize] = IndexBuffer::new();
        for buffer in self.gpu_buffers.iter_mut() {
            if buffer.buffer.get_handle() == view.handle {
                buffer.free(BufferSpace {
                    offset: view.offset,
                    size: view.size,
                });
            }
        }
    }

    pub fn get_index_buffer(&mut self, handle: IndexBufferHandle) -> Option<&IndexBuffer> {
        if handle.get_id() > MAX_INDEX_BUFFERS as _ {
            error!(
                "[VertexBufferManager]: Get index buffer failed: index buffer index out of bounds"
            );
            return None;
        }

        Some(&self.index_buffers[handle.get_id() as usize])
    }
}

#[derive(Copy, Clone, Debug)]
pub struct VertexBuffer {
    pub view: BufferView,
    pub attributes: [Attribute; MAX_ATTRIBUTES],
    pub offsets: [u32; MAX_ATTRIBUTES],
    pub count: u32,
    pub vertex_size: u32,
    pub base_vertex: u32,
}

impl VertexBuffer {
    fn new() -> Self {
        Self {
            view: BufferView::new(),
            attributes: [Attribute {
                location: 0,
                ty: AttributeType::None,
                num: 0,
                normalize: false,
            }; MAX_ATTRIBUTES],
            offsets: [0; MAX_ATTRIBUTES],
            count: 0,
            vertex_size: 0,
            base_vertex: 0,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct IndexBuffer {
    pub view: BufferView,
    pub ty: IndexType,
    pub count: u32,
}

impl IndexBuffer {
    fn new() -> Self {
        Self {
            view: BufferView::new(),
            ty: IndexType::U16,
            count: 0,
        }
    }
}

struct GPUBuffer<'a> {
    buffer: Buffer<'a>,
    free_space: Vec<BufferSpace>,
}

impl<'a> GPUBuffer<'a> {
    fn new() -> Self {
        Self {
            buffer: Buffer::create(GPU_BUFFER_SIZE as _, Access::None, false),
            free_space: vec![BufferSpace {
                offset: 0,
                size: GPU_BUFFER_SIZE as _,
            }],
        }
    }

    fn allocate(&mut self, size: u32, alignment: u32) -> BufferSpace {
        for (i, space) in (0u32..).zip(self.free_space.iter_mut()) {
            if space.size < size {
                continue;
            }

            let mod_ = if alignment == 0 {
                0
            } else {
                space.offset % alignment
            };
            let diff = alignment - mod_;
            if mod_ == 0 {
                let s = BufferSpace {
                    offset: space.offset,
                    size: size,
                };
                if space.size == size {
                    self.free_space.remove(i as usize);
                } else if space.size > size {
                    space.offset += size;
                    space.size -= size;
                }
                return s;
            } else if diff <= (space.size - size) {
                let s = BufferSpace {
                    offset: space.offset + diff,
                    size,
                };
                if space.size > (size + diff) {
                    let old_size = space.size;
                    space.size = diff;
                    let offset = space.offset + space.size + size;
                    let new_size = old_size - space.size - size;
                    self.free_space.push(BufferSpace {
                        offset,
                        size: new_size,
                    });
                    self.free_space.sort();
                } else if space.size == (size + diff) {
                    space.size = diff;
                }
                return s;
            }
        }
        BufferSpace { offset: 0, size: 0 }
    }

    fn free(&mut self, space: BufferSpace) {
        self.free_space.push(space);
        self.optimize();
    }

    fn optimize(&mut self) {
        self.free_space.sort();
        let mut to_swap = Vec::new();
        let mut to_remove = Vec::new();

        for (i, s) in self.free_space.iter().enumerate().rev() {
            if i > 0 {
                let mut prev = self.free_space[i - 1];
                let offset = s.offset;
                if prev.offset + prev.size == offset {
                    prev.size += s.size;
                    to_swap.push((i - 1, prev));
                    to_remove.push(i);
                    //self.free_space[i-1] = prev;
                    //self.free_space.remove(i);
                }
            }
        }

        for (i, s) in to_swap.iter() {
            self.free_space[*i] = *s;
        }
        for i in to_remove.iter() {
            self.free_space.remove(*i);
        }
    }
}

fn vertex_size(attributes: &[Attribute]) -> u32 {
    let mut size = 0;
    for attr in attributes {
        size += attr.get_size();
    }
    size
}

trait VertexUtils {
    fn get_size(&self) -> u32;
}

impl VertexUtils for Attribute {
    fn get_size(&self) -> u32 {
        match self.ty {
            AttributeType::None => 1 * self.num as u32,
            AttributeType::U8 => 1 * self.num as u32,
            AttributeType::F32 => 4 * self.num as u32,
        }
    }
}

fn calculate_offsets(offsets: &mut [u32], attributes: &[Attribute]) {
    let mut offset = 0;
    for (i, attr) in attributes.iter().enumerate() {
        offsets[i] = offset;
        offset += attr.get_size();
    }
}

impl VertexUtils for IndexType {
    fn get_size(&self) -> u32 {
        match self {
            Self::U16 => 2,
            Self::U32 => 4,
        }
    }
}
