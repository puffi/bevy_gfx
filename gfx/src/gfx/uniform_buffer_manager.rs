use super::buffer::{Access, Buffer};
use gfx_common::config::{MAX_UNIFORM_BUFFERS};
use gfx_common::UniformBufferHandle;
use log::error;

pub struct UniformBufferManager<'a> {
    uniform_buffers: [UniformBuffer<'a>; MAX_UNIFORM_BUFFERS],
}

impl<'a> UniformBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            uniform_buffers: {
                let mut arr: [UniformBuffer<'a>; MAX_UNIFORM_BUFFERS] =
                    unsafe { std::mem::MaybeUninit::uninit().assume_init() };
                for item in arr.iter_mut() {
                    unsafe {
                        std::ptr::write(item, UniformBuffer::default());
                    }
                }
                arr
            },
        }
    }

    pub fn create_uniform_buffer(&mut self, size: u32) -> UniformBufferHandle {
        let size = size + (16 - size % 16); // align to 16 bytes

        for (i, ub) in (0u16..).zip(self.uniform_buffers.iter_mut()) {
            if ub.buffer.is_valid() {
                continue;
            }

            ub.buffer = Buffer::create(size, Access::None, false);

            return UniformBufferHandle::new(i);
        }

        error!("[UniformBufferManager]: Reached uniform buffer limit. Can't create a new one");
        UniformBufferHandle::invalid()
    }

    pub fn destroy_uniform_buffer(&mut self, handle: UniformBufferHandle) {
        if handle.get_id() > MAX_UNIFORM_BUFFERS as _ {
            error!("[UniformBufferManager]: Destroy uniform buffer failed: uniform buffer index out of bounds");
            return;
        }

        self.uniform_buffers[handle.get_id() as usize].buffer.destroy();
        self.uniform_buffers[handle.get_id() as usize] = UniformBuffer::default();
    }

    pub fn get_uniform_buffer(&self, handle: UniformBufferHandle) -> Option<&UniformBuffer> {
        if handle.get_id() > MAX_UNIFORM_BUFFERS as _ {
            error!("[UniformBufferManager]: Get uniform buffer failed: uniform buffer index out of bounds");
            return None;
        }

        Some(&self.uniform_buffers[handle.get_id() as usize])
    }
}

#[derive(Default)]
pub struct UniformBuffer<'a> {
    pub buffer: Buffer<'a>,
}
