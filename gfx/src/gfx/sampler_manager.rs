use gfx_common::config::MAX_SAMPLERS;
use gfx_common::{SamplerHandle, Wrap, Filter};
use ::gl;

use log::{info, debug};

pub struct SamplerManager {
    samplers: [Sampler; MAX_SAMPLERS],
}

impl SamplerManager {
    pub fn new() -> Self {
        Self {
            samplers: [Sampler::new(); MAX_SAMPLERS],
        }
    }

    pub fn get_sampler_from_properties(&mut self, min: Filter, mag: Filter, wrap_u: Wrap, wrap_v: Wrap, wrap_w: Wrap) -> SamplerHandle {
        let hash = hash(min, mag, wrap_u, wrap_v, wrap_w);

        let sampler = &mut self.samplers[hash as usize];
        if !sampler.is_valid() {
            debug!("[SamplerManager]: Creating sampler from properties min: {:?}, mag: {:?}, wrap_u: {:?}, wrap_v: {:?}, wrap_w: {:?}", min, mag, wrap_u, wrap_v, wrap_w);
            unsafe { gl::CreateSamplers(1, &mut sampler.handle); }
            unsafe { gl::SamplerParameteri(sampler.handle, gl::TEXTURE_MIN_FILTER, min.gl_type() as _); }
            unsafe { gl::SamplerParameteri(sampler.handle, gl::TEXTURE_MAG_FILTER, mag.gl_type() as _); }
            unsafe { gl::SamplerParameteri(sampler.handle, gl::TEXTURE_WRAP_S, wrap_u.gl_type() as _); }
            unsafe { gl::SamplerParameteri(sampler.handle, gl::TEXTURE_WRAP_T, wrap_v.gl_type() as _); }
            unsafe { gl::SamplerParameteri(sampler.handle, gl::TEXTURE_WRAP_R, wrap_w.gl_type() as _); }
        }

        SamplerHandle::new(hash)
    }

    pub fn get_sampler(&self, handle: SamplerHandle) -> Option<&Sampler> {
        if handle.get_id() >= MAX_SAMPLERS as _ {
            info!("[SamplerManager]: Get sampler failed: sampler index out of bounds");
            return None;
        }

        if !self.samplers[handle.get_id() as usize].is_valid() {
            info!("[SamplerManager]: Get sampler failed: invalid sampler");
            return None;
        }

        Some(&self.samplers[handle.get_id() as usize])
    }
}

#[derive(Debug,Copy,Clone,PartialEq, Eq)]
pub struct Sampler {
    handle: u32,
}

impl Sampler {
    fn new() -> Self {
        Self {
            handle: 0,
        }
    }

    fn is_valid(&self) -> bool {
        self.handle != 0
    }

    pub fn handle(&self) -> u32 {
        self.handle
    }
}

const FILTER_SHIFT_MIN: u16 = 0;
const FILTER_SHIFT_MAG: u16 = 1;
const WRAP_SHIFT_U: u16 = 2;
const WRAP_SHIFT_V: u16 = 4;
const WRAP_SHIFT_W: u16 = 6;

fn hash(min: Filter, mag: Filter, wrap_u: Wrap, wrap_v: Wrap, wrap_w: Wrap) -> u16 {
    let mut hash = min.value() << FILTER_SHIFT_MIN;
    hash |= mag.value() << FILTER_SHIFT_MAG;
    hash |= wrap_u.value() << WRAP_SHIFT_U;
    hash |= wrap_v.value() << WRAP_SHIFT_V;
    hash |= wrap_w.value() << WRAP_SHIFT_W;
    hash
}

trait TypeUtil {
    fn value(&self) -> u16;
    fn gl_type(&self) -> gl::types::GLenum;
}

impl TypeUtil for Filter {
    fn value(&self) -> u16 {
        match self {
            Self::Nearest => 0,
            Self::Linear => 1,
        }
    }

    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::Nearest => gl::NEAREST,
            Self::Linear => gl::LINEAR,
        }
    }
}

impl TypeUtil for Wrap {
    fn value(&self) -> u16 {
        match self {
            Self::Repeat => 0,
            Self::MirroredRepeat => 1,
            Self::Clamp => 2,
        }
    }

    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::Repeat => gl::REPEAT,
            Self::MirroredRepeat => gl::MIRRORED_REPEAT,
            Self::Clamp => gl::CLAMP_TO_EDGE,
        }
    }
}
