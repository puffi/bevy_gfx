use super::buffer::{Access, Buffer, BufferSpace, BufferView};
use gfx_common::config::STAGING_BUFFER_SIZE;
use ::gl;

pub struct StagingBufferManager<'a> {
    write_buffers: Vec<StagingBuffer<'a>>,
    current_frame: usize,
}

impl<'a> StagingBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            write_buffers: Vec::new(),
            current_frame: 0,
        }
    }

    pub fn update_frame(&mut self, frame: usize) {
        self.current_frame = frame;

        self.reset_spaces();
    }

    fn reset_spaces(&mut self) {
        for buffer in self.write_buffers.iter_mut() {
            buffer.free_space[self.current_frame as usize] = STAGING_BUFFER_SIZE as _;
        }
    }

    pub fn upload(&mut self, data: &[u8], view: BufferView) {
        if data.len() > STAGING_BUFFER_SIZE {
            let mut buffer = Buffer::create(data.len() as u32, Access::Write, false);
            let handle = buffer.get_handle();
            if let Some(mapped_data) = &mut buffer.mapped_data {
                mapped_data.clone_from_slice(data);
            } else {
                assert!(false);
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    handle,
                    view.handle,
                    0,
                    view.offset as _,
                    view.size as _,
                );
            }
            buffer.destroy();
            return;
        }

        for buffer in self.write_buffers.iter_mut() {
            let space = buffer.allocate(data.len() as u32, self.current_frame);
            if !space.is_valid() {
                continue;
            }

            if let Some(mapped_data) = &mut buffer.buffer.mapped_data {
                let offset =
                    space.offset as usize + self.current_frame as usize * STAGING_BUFFER_SIZE;
                let to_offset = offset + space.size as usize;
                mapped_data[offset..to_offset].clone_from_slice(data);
            } else {
                assert!(false);
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    buffer.buffer.get_handle(),
                    view.handle,
                    space.offset as _,
                    view.offset as _,
                    space.size as _,
                );
            }
            return;
        }

        self.write_buffers.push(StagingBuffer::new());
        self.upload(data, view);
    }
}

struct StagingBuffer<'a> {
    buffer: Buffer<'a>,
    free_space: [u32; 3],
}

impl<'a> StagingBuffer<'a> {
    fn new() -> Self {
        let buffer = Buffer::create(STAGING_BUFFER_SIZE as _, Access::Write, true);
        let free_space = [STAGING_BUFFER_SIZE as u32; 3];
        Self { buffer, free_space }
    }

    fn allocate(&mut self, size: u32, frame: usize) -> BufferSpace {
        if self.free_space[frame] < size {
            return BufferSpace { offset: 0, size: 0 };
        }

        let space = BufferSpace {
            offset: STAGING_BUFFER_SIZE as u32 - self.free_space[frame],
            size: size,
        };
        self.free_space[frame] -= size;
        space
    }
}
