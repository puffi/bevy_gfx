use gfx_common::{Format, TextureHandle, TextureType};
use gfx_common::config::MAX_TEXTURES;
use ::gl;

use log::{debug, error, info};

pub struct TextureManager {
    textures: [Texture; MAX_TEXTURES],
}

impl TextureManager {
    pub fn new() -> Self {
        Self {
            textures: [Texture::new(); MAX_TEXTURES],
        }
    }

    pub fn create_texture_2d(
        &mut self,
        width: u32,
        height: u32,
        fmt: Format,
        mip_levels: u32,
    ) -> TextureHandle {
        for (i, tex) in (0u16..).zip(self.textures.iter_mut()) {
            if tex.is_valid() {
                continue;
            }

            unsafe {
                gl::CreateTextures(gl::TEXTURE_2D, 1, &mut tex.handle);
            }
            unsafe {
                gl::TextureStorage2D(
                    tex.handle,
                    mip_levels as _,
                    fmt.gl_type(),
                    width as _,
                    height as _,
                );
            }

            tex.ty = TextureType::Single2D;
            tex.format = fmt;
            tex.width = width;
            tex.height = height;
            tex.depth = 1;
            tex.mip_levels = mip_levels;

            return TextureHandle::new(i);
        }

        info!("[TextureManager]: Texture limit reached, can't allocate a new one");
        TextureHandle::invalid()
    }

    pub fn destroy_texture(&mut self, handle: TextureHandle) {
        if handle.get_id() >= MAX_TEXTURES as _ {
            error!("[TextureManager]: Destroy texture failed: texture index out of bounds");
            return;
        }

        unsafe {
            gl::DeleteTextures(1, &self.textures[handle.get_id() as usize].handle);
        }
        self.textures[handle.get_id() as usize] = Texture::new();
    }

    pub fn get_texture(&self, handle: TextureHandle) -> Option<&Texture> {
        if handle.get_id() >= MAX_TEXTURES as _ {
            debug!("[TextureManager]: Get texture failed: texture index out of bounds");
            return None;
        }

        if !self.textures[handle.get_id() as usize].is_valid() {
            debug!("[TextureManager]: Get texture failed: invalid texture");
            return None;
        }

        Some(&self.textures[handle.get_id() as usize])
    }

    pub fn update_texture(
        &self,
        handle: TextureHandle,
        mip_level: u32,
        offset_x: u32,
        offset_y: u32,
        _offset_z: u32,
        width: u32,
        height: u32,
        _depth: u32,
        data: &[u8],
    ) {
        if handle.get_id() >= MAX_TEXTURES as _ {
            error!("[TextureManager]: Update texture failed: texture index out of bounds");
            return;
        }

        let tex = &self.textures[handle.get_id() as usize];
        if !tex.is_valid() {
            error!("[TextureManager]: Update texture failed: invalid texture");
            return;
        }

        match tex.ty {
            TextureType::Single1D => assert!(false, "1D texture is not yet implemented"),
            TextureType::Single2D => unsafe {
                gl::TextureSubImage2D(
                    tex.handle,
                    mip_level as _,
                    offset_x as _,
                    offset_y as _,
                    width as _,
                    height as _,
                    tex.format.gl_format(),
                    tex.format.gl_base_type(),
                    data.as_ptr() as _,
                );
            },
            TextureType::Single3D => assert!(false, "3D texture is not yet implemented"),
            TextureType::Array1D => assert!(false, "1D array texture is not yet implemented"),
            TextureType::Array2D => assert!(false, "2D array texture is not yet implemented"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Texture {
    ty: TextureType,
    format: Format,
    handle: u32,
    width: u32,
    height: u32,
    depth: u32,
    mip_levels: u32,
}

impl Texture {
    fn new() -> Self {
        Self {
            ty: TextureType::Single1D,
            format: Format::R8,
            handle: 0,
            width: 0,
            height: 0,
            depth: 0,
            mip_levels: 0,
        }
    }

    pub fn handle(&self) -> u32 {
        self.handle
    }

    pub fn ty(&self) -> TextureType {
        self.ty
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0
    }

    pub fn size(&self) -> u32 {
        self.width * self.height * self.depth
    }
}

trait GlUtils {
    fn gl_type(&self) -> gl::types::GLenum;
    fn gl_format(&self) -> gl::types::GLenum;
    fn gl_base_type(&self) -> gl::types::GLenum;
}

impl GlUtils for Format {
    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::R8,
            Self::RGB8 => gl::RGB8,
            Self::RGBA8 => gl::RGBA8,
            Self::RGB16F => gl::RGB16F,
            Self::RGBA16F => gl::RGBA16F,
            Self::RGB32F => gl::RGB32F,
            Self::RGBA32F => gl::RGBA32F,
            Self::Depth32 => gl::DEPTH_COMPONENT32F,
            Self::Depth32Stencil8 => gl::DEPTH32F_STENCIL8,
        }
    }

    fn gl_format(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::RED,
            Self::RGB8 => gl::RGB,
            Self::RGBA8 => gl::RGBA,
            Self::RGB16F => gl::RGB,
            Self::RGBA16F => gl::RGBA,
            Self::RGB32F => gl::RGB,
            Self::RGBA32F => gl::RGBA,
            Self::Depth32 => gl::DEPTH_COMPONENT,
            Self::Depth32Stencil8 => gl::DEPTH_STENCIL,
        }
    }

    fn gl_base_type(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::UNSIGNED_BYTE,
            Self::RGB8 => gl::UNSIGNED_BYTE,
            Self::RGBA8 => gl::UNSIGNED_BYTE,
            Self::RGB16F => gl::FLOAT,
            Self::RGBA16F => gl::FLOAT,
            Self::RGB32F => gl::FLOAT,
            Self::RGBA32F => gl::FLOAT,
            Self::Depth32 => gl::FLOAT,
            Self::Depth32Stencil8 => gl::FLOAT_32_UNSIGNED_INT_24_8_REV,
        }
    }
}
