use gfx_common::Attachment;
use ::gl;
use ::gl::types::GLenum;

pub fn attachment_gl_type(att: &Attachment) -> GLenum {
    match att {
        Attachment::Color0 => gl::COLOR_ATTACHMENT0,
        Attachment::Color1 => gl::COLOR_ATTACHMENT1,
        Attachment::Color2 => gl::COLOR_ATTACHMENT2,
        Attachment::Color3 => gl::COLOR_ATTACHMENT3,
        Attachment::Color4 => gl::COLOR_ATTACHMENT4,
        Attachment::Color5 => gl::COLOR_ATTACHMENT5,
        Attachment::Color6 => gl::COLOR_ATTACHMENT6,
        Attachment::Color7 => gl::COLOR_ATTACHMENT7,
        Attachment::Depth => gl::DEPTH_ATTACHMENT,
        Attachment::DepthStencil => gl::DEPTH_STENCIL_ATTACHMENT,
        Attachment::Stencil => gl::STENCIL_ATTACHMENT,
    }
}
