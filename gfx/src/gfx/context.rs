use std::ffi::c_void;

use ::egl::*;
use gfx_common::{Platform, PlatformHandle};
use gl;
use log::{debug, error, info};

pub struct Context {
    display: EGLDisplay,
    surface: EGLSurface,
    context: EGLContext,
    native_display: EGLNativeDisplayType,
    native_window: EGLNativeWindowType,
}

impl Context {
    pub fn new() -> Self {
        Self {
            display: std::ptr::null_mut(),
            surface: std::ptr::null_mut(),
            context: std::ptr::null_mut(),
            native_display: std::ptr::null_mut(),
            native_window: std::ptr::null_mut(),
        }
    }

    pub fn initialize(&mut self, win: &dyn Platform) -> bool {
        let (window, dpy, egl_win): (
            EGLNativeWindowType,
            EGLNativeDisplayType,
            Option<*mut c_void>,
        ) = match win.get_platform_handle() {
            PlatformHandle::Win32 {
                hwnd,
                hdc,
                hinstance: _,
            } => (
                hwnd as EGLNativeWindowType,
                hdc as EGLNativeDisplayType,
                None,
            ),
            PlatformHandle::Xlib { window, display } => (
                window as EGLNativeWindowType,
                display as EGLNativeDisplayType,
                None,
            ),
            #[cfg(feature = "wayland")]
            PlatformHandle::Wayland {
                surface, display, ..
            } => {
                use wayland_sys::egl::WAYLAND_EGL_HANDLE;
                use wayland_sys::ffi_dispatch;
                let size = win.get_window_size();
                let egl_window = unsafe {
                    ffi_dispatch!(
                        WAYLAND_EGL_HANDLE,
                        wl_egl_window_create,
                        surface as _,
                        size.0,
                        size.1
                    )
                };
                debug!("egl_window: {:X?}", egl_window);
                (
                    surface as EGLNativeWindowType,
                    display as EGLNativeDisplayType,
                    Some(egl_window as _),
                )
            }
            #[cfg(not(feature = "wayland"))]
            PlatformHandle::Wayland { .. } => panic!("Wayland requires the feature 'wayland'."),
        };
        self.native_display = dpy as _;
        self.native_window = window as _;

        self.display = unsafe { eglGetDisplay(self.native_display) };
        if self.display == std::ptr::null_mut() {
            error!("[EGLContext]: No Display connection found: {:X}", unsafe {
                eglGetError()
            });
            return false;
        } else {
            info!("[EGLContext]: Found display connection");
        }

        let mut major = 0;
        let mut minor = 0;
        let success = unsafe { eglInitialize(self.display, &mut major as _, &mut minor as _) };
        if success == 0 {
            error!(
                "[EGLContext]: Failed to initialize display connection: {:X}",
                unsafe { eglGetError() }
            );
            return false;
        } else {
            info!("[EGLContext]: Initialized display connection");
            info!("[EGLContext]: EGL version: {}.{}", major, minor);
        }
        if major == 1 && minor < 4 {
            error!("[EGLContext]: EGL version does not match 1.4 or higher");
            return false;
        }
        if major == 1 && minor == 4 {
            info!("[EGLContext]: Querying EGL_KHR_client_get_all_proc_addresses support");
            let exts = unsafe { eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS) };
            let exts = unsafe { std::ffi::CStr::from_ptr(exts) };
            let exts = match exts.to_str() {
                Ok(s) => s,
                Err(e) => {
                    error!("[EGLContext]: Failed to convert extension string: {}", e);
                    return false;
                }
            };
            if !exts.contains("EGL_KHR_client_get_all_proc_addresses") {
                error!("[EGLContext]: EGL_KHR_client_get_all_proc_addresses is not supported");
                return false;
            }
        }

        let success = unsafe { eglBindAPI(EGL_OPENGL_API as _) };
        if success == 0 {
            error!("[EGLContext]: Failed to set API to OpenGL: {:X}", unsafe {
                eglGetError()
            });
            return false;
        } else {
            info!("[EGLContext]: Successfully set API to OpenGL");
        }

        let mut config = std::ptr::null_mut();
        let mut num_configs = 0;
        let success = unsafe {
            eglChooseConfig(
                self.display,
                SURFACE_ATTRIBS.as_ptr(),
                &mut config,
                1,
                &mut num_configs,
            )
        };
        if success == 0 {
            error!("[EGLContext]: No config found: {:X}", unsafe {
                eglGetError()
            });
            return false;
        } else {
            info!("[EGLContext]: Acquired a config");
        }

        if let Some(egl_window) = egl_win {
            self.surface = unsafe {
                eglCreateWindowSurface(self.display, config, egl_window as _, std::ptr::null())
            };
        } else {
            self.surface = unsafe {
                eglCreateWindowSurface(self.display, config, self.native_window, std::ptr::null())
            };
        }
        if self.surface == std::ptr::null_mut() {
            error!("[EGLContext]: Failed to create surface: {:X}", unsafe {
                eglGetError()
            });
            return false;
        } else {
            info!("[EGLContext]: Created surface");
        }

        if major == 1 && minor == 4 {
            info!("[EGLContext]: EGL version < 1.5, trying KHR attributes");
            self.context = unsafe {
                eglCreateContext(
                    self.display,
                    config,
                    EGL_NO_CONTEXT,
                    CONTEXT_ATTRIBS_1_4.as_ptr(),
                )
            };
        } else {
            self.context = unsafe {
                eglCreateContext(
                    self.display,
                    config,
                    EGL_NO_CONTEXT,
                    CONTEXT_ATTRIBS.as_ptr(),
                )
            };
        }
        if self.context == std::ptr::null_mut() {
            error!("[EGLContext]: Failed to create context: {:X}", unsafe {
                eglGetError()
            });
            return false;
        } else {
            info!("[EGLContext]: Created context");
        }

        true
    }

    pub fn terminate(&self) {
        if self.context != std::ptr::null_mut() {
            unsafe {
                eglMakeCurrent(self.display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            }
        }

        unsafe {
            eglTerminate(self.display);
        }
        info!("[EGLContext]: Terminating EGL");
    }

    pub fn make_current(&self) -> bool {
        let success =
            unsafe { eglMakeCurrent(self.display, self.surface, self.surface, self.context) };
        if success == 0 {
            error!(
                "[EGLContext]: Failed to make context current: {:X}",
                unsafe { eglGetError() }
            );
            return false;
        } else {
            info!("[EGLContext]: Context made current");
            return true;
        }
    }

    pub fn swap_buffers(&self) {
        unsafe {
            eglSwapBuffers(self.display, self.surface);
        }
    }

    pub fn set_swap_interval(&self, value: i32) {
        unsafe {
            eglSwapInterval(self.display, value);
        }
    }

    pub fn load_gl() {
        let f = Self::get_proc_address("glCreateVertexArrays");
        info!("[EGLContext]: CreateVertexArrays: {:?}", f);
        gl::load_with(|s| Self::get_proc_address(s));
    }

    fn get_proc_address<T: AsRef<str>>(name: T) -> *const std::os::raw::c_void {
        let cstr = std::ffi::CString::new(name.as_ref()).unwrap();
        unsafe { eglGetProcAddress(cstr.as_ptr()) }
    }

    pub fn setup_debug_callback(&self) {
        #[cfg(debug_assertions)]
        {
            unsafe {
                gl::Enable(gl::DEBUG_OUTPUT);
                gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
                gl::DebugMessageCallback(Some(log_gl_message), std::ptr::null());
            }
        }
    }
}

const SURFACE_ATTRIBS: [EGLint; 13] = [
    EGL_RED_SIZE,
    8,
    EGL_GREEN_SIZE,
    8,
    EGL_BLUE_SIZE,
    8,
    EGL_DEPTH_SIZE,
    24,
    EGL_STENCIL_SIZE,
    8,
    EGL_CONFORMANT,
    EGL_OPENGL_BIT,
    EGL_NONE,
];

#[cfg(debug_assertions)]
const DEBUG_CONTEXT: EGLint = EGL_TRUE;
#[cfg(not(debug_assertions))]
const DEBUG_CONTEXT: EGLint = EGL_FALSE;

const CONTEXT_ATTRIBS: [EGLint; 9] = [
    EGL_CONTEXT_MAJOR_VERSION,
    4,
    EGL_CONTEXT_MINOR_VERSION,
    5,
    EGL_CONTEXT_OPENGL_PROFILE_MASK,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
    EGL_CONTEXT_OPENGL_DEBUG,
    DEBUG_CONTEXT,
    EGL_NONE,
];

#[cfg(debug_assertions)]
const DEBUG_FLAG_1_4: EGLint = EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR;
#[cfg(not(debug_assertions))]
const DEBUG_FLAG_1_4: EGLint = 0;

const CONTEXT_ATTRIBS_1_4: [EGLint; 9] = [
    EGL_CONTEXT_MAJOR_VERSION_KHR,
    4,
    EGL_CONTEXT_MINOR_VERSION_KHR,
    5,
    EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR,
    EGL_CONTEXT_FLAGS_KHR,
    DEBUG_FLAG_1_4,
    EGL_NONE,
];

extern "system" fn log_gl_message(
    src: gl::types::GLenum,
    ty: gl::types::GLenum,
    id: gl::types::GLuint,
    severity: gl::types::GLenum,
    length: gl::types::GLsizei,
    msg: *const gl::types::GLchar,
    _user_param: *mut std::os::raw::c_void,
) {
    debug!(
        "[GLDebug]: Source: {}, Type: {}, Id: {}, Severity: {}, Length: {}",
        source_to_string(src),
        type_to_string(ty),
        id,
        severity_to_string(severity),
        length
    );
    if let Ok(msg) = unsafe { std::ffi::CStr::from_ptr(msg) }.to_str() {
        debug!("[GLDebug]: {}", msg);
    } else {
        debug!("[GLDebug]: Error message contains invalid UTF-8");
    }
}

fn source_to_string(src: gl::types::GLenum) -> &'static str {
    match src {
        gl::DEBUG_SOURCE_API => return "GL_DEBUG_SOURCE_API",
        gl::DEBUG_SOURCE_WINDOW_SYSTEM => return "GL_DEBUG_SOURCE_WINDOW_SYSTEM",
        gl::DEBUG_SOURCE_SHADER_COMPILER => return "GL_DEBUG_SOURCE_SHADER_COMPILER",
        gl::DEBUG_SOURCE_THIRD_PARTY => return "GL_DEBUG_SOURCE_THIRD_PARTY",
        gl::DEBUG_SOURCE_APPLICATION => return "GL_DEBUG_SOURCE_APPLICATION",
        gl::DEBUG_SOURCE_OTHER => return "GL_DEBUG_SOURCE_OTHER",
        _ => return "Unknown source",
    }
}

fn type_to_string(ty: gl::types::GLenum) -> &'static str {
    match ty {
        gl::DEBUG_TYPE_ERROR => return "GL_DEBUG_TYPE_ERROR",
        gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR",
        gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR",
        gl::DEBUG_TYPE_PORTABILITY => return "GL_DEBUG_TYPE_PORTABILITY",
        gl::DEBUG_TYPE_PERFORMANCE => return "GL_DEBUG_TYPE_PERFORMANCE",
        gl::DEBUG_TYPE_MARKER => return "GL_DEBUG_TYPE_MARKER",
        gl::DEBUG_TYPE_PUSH_GROUP => return "GL_DEBUG_TYPE_PUSH_GROUP",
        gl::DEBUG_TYPE_POP_GROUP => return "GL_DEBUG_TYPE_POP_GROUP",
        gl::DEBUG_TYPE_OTHER => return "GL_DEBUG_TYPE_OTHER",
        _ => return "Unknown type",
    }
}

fn severity_to_string(severity: gl::types::GLenum) -> &'static str {
    match severity {
        gl::DEBUG_SEVERITY_HIGH => return "GL_DEBUG_SEVERITY_HIGH",
        gl::DEBUG_SEVERITY_MEDIUM => return "GL_DEBUG_SEVERITY_MEDIUM",
        gl::DEBUG_SEVERITY_LOW => return "GL_DEBUG_SEVERITY_LOW",
        gl::DEBUG_SEVERITY_NOTIFICATION => return "GL_DEBUG_SEVERITY_NOTIFICATION",
        _ => return "Unknown severity",
    }
}
