use ::gl;

pub struct SyncManager {
    sync_objects: [gl::types::GLsync; 3],
}

impl SyncManager {
    pub fn new() -> Self {
        let sync_objects = [std::ptr::null(); 3];
        Self { sync_objects }
    }

    pub fn wait_for_sync_object(&mut self, frame: usize) {
        if self.sync_objects[frame] != std::ptr::null() {
            let mut status = unsafe {
                gl::ClientWaitSync(self.sync_objects[frame], gl::SYNC_FLUSH_COMMANDS_BIT, 0)
            };
            while (status & gl::ALREADY_SIGNALED) == 0 {
                status = unsafe {
                    gl::ClientWaitSync(self.sync_objects[frame], gl::SYNC_FLUSH_COMMANDS_BIT, 0)
                };
            }
            unsafe {
                gl::DeleteSync(self.sync_objects[frame]);
            }
            self.sync_objects[frame] = std::ptr::null();
        }
    }

    pub fn set_sync_point(&mut self, frame: usize) {
        self.sync_objects[frame] = unsafe { gl::FenceSync(gl::SYNC_GPU_COMMANDS_COMPLETE, 0) };
    }
}
