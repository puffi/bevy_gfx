use super::buffer::BufferView;
use gfx_common::*;
use super::context::*;
use super::draw_state_manager::{DrawStateManager, StorageBinding, UniformBinding, TextureBinding};
use super::rt_manager::RenderTargetManager;
use super::sampler_manager::SamplerManager;
use super::shader_manager::ShaderManager;
use super::staging_buffer_manager::StagingBufferManager;
use super::storage_buffer_manager::StorageBufferManager;
use super::sync_manager::*;
use super::texture_manager::TextureManager;
use super::uniform_buffer_manager::UniformBufferManager;
use super::vertex_buffer_manager::VertexBufferManager;
use log::error;

#[cfg(feature = "sdl2_context")]
use sdl2_sys::sdl::video::SDL_Window;

pub struct Gfx<'a> {
    context: Context,
    sync_manager: SyncManager,
    rt_manager: RenderTargetManager,
    shader_manager: ShaderManager,
    staging_buffer_manager: StagingBufferManager<'a>,
    draw_state_manager: DrawStateManager,
    vertex_buffer_manager: VertexBufferManager<'a>,
    uniform_buffer_manager: UniformBufferManager<'a>,
    storage_buffer_manager: StorageBufferManager<'a>,
    texture_manager: TextureManager,
    sampler_manager: SamplerManager,

    current_present_mode: PresentMode,

    current_frame: usize,
}

impl<'a> Gfx<'a> {
    pub fn new() -> Self {
            Self {
                context: Context::new(),
                sync_manager: SyncManager::new(),
                rt_manager: RenderTargetManager::new(),
                shader_manager: ShaderManager::new(),
                staging_buffer_manager: StagingBufferManager::new(),
                draw_state_manager: DrawStateManager::new(),
                vertex_buffer_manager: VertexBufferManager::new(),
                uniform_buffer_manager: UniformBufferManager::new(),
                storage_buffer_manager: StorageBufferManager::new(),
                texture_manager: TextureManager::new(),
                sampler_manager: SamplerManager::new(),
                current_present_mode: PresentMode::VSync, // default for eglSwapInterval
                current_frame: 0,
            }
    }

    #[cfg(feature = "egl_context")]
    pub fn initialize(&mut self, win: &dyn Platform) -> bool {
        if !self.context.initialize(win) {
            return false;
        }
        if !self.context.make_current() {
            return false;
        }
        Context::load_gl();
        self.context.setup_debug_callback();
        if !self.draw_state_manager.initialize() {
            return false;
        }
        return true;
    }

    #[cfg(feature = "sdl2_context")]
    pub fn initialize(&mut self, win: *mut SDL_Window) -> bool {
        if !self.context.initialize(win) {
            return false;
        }
        if !self.context.make_current() {
            return false;
        }
        Context::load_gl();
        self.context.setup_debug_callback();
        if !self.draw_state_manager.initialize() {
            return false;
        }
        return true;
    }

    pub fn present(&mut self, mode: PresentMode) {
        if mode != self.current_present_mode {
            self.current_present_mode = mode;
            let value = match self.current_present_mode {
                PresentMode::Immediate => 0,
                PresentMode::VSync => 1,
            };
            self.context.set_swap_interval(value);
        }
        self.sync_manager.set_sync_point(self.current_frame);

        self.context.swap_buffers();

        self.update_frame();
    }

    pub fn clear_render_target(&self, handle: RenderTargetHandle) {
        self.rt_manager.clear_render_target(handle);
    }

    pub fn clear_render_target_attachment_clearvalue(
        &self,
        handle: RenderTargetHandle,
        att: &Attachment,
        cv: &ClearValue,
    ) {
        self.rt_manager
            .clear_render_target_attachment_clearvalue(handle, att, cv);
    }

    pub fn create_vertex_shader(&mut self, data: &[u8]) -> VertexShaderHandle {
        self.shader_manager.create_vertex_shader(data)
    }

    pub fn destroy_vertex_shader(&mut self, handle: VertexShaderHandle) {
        self.shader_manager.destroy_vertex_shader(handle);
    }

    pub fn create_fragment_shader(&mut self, data: &[u8]) -> FragmentShaderHandle {
        self.shader_manager.create_fragment_shader(data)
    }

    pub fn destroy_fragment_shader(&mut self, handle: FragmentShaderHandle) {
        self.shader_manager.destroy_fragment_shader(handle);
    }

    pub fn create_vertex_buffer(
        &mut self,
        data: &[u8],
        attributes: &[Attribute],
    ) -> VertexBufferHandle {
        let handle = self
            .vertex_buffer_manager
            .create_vertex_buffer(data, attributes);
        if let Some(vb) = self.vertex_buffer_manager.get_vertex_buffer(handle) {
            self.staging_buffer_manager.upload(data, vb.view);
        }
        handle
    }

    pub fn destroy_vertex_buffer(&mut self, handle: VertexBufferHandle) {
        self.vertex_buffer_manager.destroy_vertex_buffer(handle);
    }

    pub fn create_index_buffer(&mut self, data: &[u8], ty: IndexType) -> IndexBufferHandle {
        let handle = self.vertex_buffer_manager.create_index_buffer(data, ty);
        if let Some(ib) = self.vertex_buffer_manager.get_index_buffer(handle) {
            self.staging_buffer_manager.upload(data, ib.view);
        }
        handle
    }

    pub fn destroy_index_buffer(&mut self, handle: IndexBufferHandle) {
        self.vertex_buffer_manager.destroy_index_buffer(handle);
    }

    pub fn create_uniform_buffer(&mut self, size: u32) -> UniformBufferHandle {
        self.uniform_buffer_manager.create_uniform_buffer(size)
    }

    pub fn destroy_uniform_buffer(&mut self, handle: UniformBufferHandle) {
        self.uniform_buffer_manager.destroy_uniform_buffer(handle)
    }

    pub fn update_uniform_buffer(&mut self, handle: UniformBufferHandle, data: &[u8], offset: u32) {
        if let Some(ub) = self.uniform_buffer_manager.get_uniform_buffer(handle) {
            if offset + data.len() as u32 > ub.buffer.size() {
                error!("[Gfx]: Update uniform buffer failed: offset + data length > buffer size");
                return;
            }

            self.staging_buffer_manager.upload(
                data,
                BufferView {
                    handle: ub.buffer.get_handle(),
                    offset,
                    size: data.len() as _,
                },
            );
        }
    }

    pub fn create_storage_buffer(&mut self, size: u32) -> StorageBufferHandle {
        self.storage_buffer_manager.create_storage_buffer(size)
    }

    pub fn destroy_storage_buffer(&mut self, handle: StorageBufferHandle) {
        self.storage_buffer_manager.destroy_storage_buffer(handle)
    }

    pub fn update_storage_buffer(&mut self, handle: StorageBufferHandle, data: &[u8], offset: u32) {
        if let Some(sb) = self.storage_buffer_manager.get_storage_buffer(handle) {
            if offset + data.len() as u32 > sb.buffer.size() {
                error!("[Gfx]: Update storage buffer failed: offset + data length > buffer size");
                return;
            }

            self.staging_buffer_manager.upload(
                data,
                BufferView {
                    handle: sb.buffer.get_handle(),
                    offset,
                    size: data.len() as _,
                },
            );
        }
    }

    pub fn create_texture_2d(
        &mut self,
        width: u32,
        height: u32,
        fmt: Format,
        mip_level: u32,
    ) -> TextureHandle {
        self.texture_manager
            .create_texture_2d(width, height, fmt, mip_level)
    }

    pub fn destroy_texture(&mut self, handle: TextureHandle) {
        self.texture_manager.destroy_texture(handle);
    }

    pub fn update_texture(
        &self,
        handle: TextureHandle,
        mip_level: u32,
        offset_x: u32,
        offset_y: u32,
        offset_z: u32,
        width: u32,
        height: u32,
        depth: u32,
        data: &[u8],
    ) {
        self.texture_manager.update_texture(
            handle, mip_level, offset_x, offset_y, offset_z, width, height, depth, data,
        );
    }

    pub fn get_sampler(&mut self, min: Filter, mag: Filter, wrap_u: Wrap, wrap_v: Wrap, wrap_w: Wrap) -> SamplerHandle {
        self.sampler_manager.get_sampler_from_properties(min, mag, wrap_u, wrap_v, wrap_w)
    }

    pub fn set_vertex_buffer(&mut self, handle: VertexBufferHandle) {
        let vb = self.vertex_buffer_manager.get_vertex_buffer(handle);
        self.draw_state_manager.set_vertex_buffer(vb);
    }

    pub fn set_index_buffer(&mut self, handle: IndexBufferHandle) {
        let ib = self.vertex_buffer_manager.get_index_buffer(handle);
        self.draw_state_manager.set_index_buffer(ib);
    }

    pub fn set_vertex_shader(&mut self, handle: VertexShaderHandle) {
        let vs = self.shader_manager.get_vertex_shader(handle);
        self.draw_state_manager.set_vertex_shader(vs);
    }

    pub fn set_fragment_shader(&mut self, handle: FragmentShaderHandle) {
        let fs = self.shader_manager.get_fragment_shader(handle);
        self.draw_state_manager.set_fragment_shader(fs);
    }

    pub fn set_blend_state(&mut self, blend_state: BlendState) {
        self.draw_state_manager.set_blend_state(blend_state);
    }

    pub fn set_depth_state(&mut self, depth_state: DepthState) {
        self.draw_state_manager.set_depth_state(depth_state);
    }

    pub fn set_cull_state(&mut self, cull_state: CullState) {
        self.draw_state_manager.set_cull_state(cull_state);
    }

    //pub fn set_scissor_state(&mut self, scissor_state: ScissorState) {
    //    self.draw_state_manager.set_scissor_state(scissor_state);
    //}

    pub fn set_viewport(&mut self, viewport: Viewport) {
        self.draw_state_manager.set_viewport(viewport);
    }

    pub fn set_uniform_buffer(
        &mut self,
        handle: UniformBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    ) {
        let mut binding = UniformBinding {
            location,
            offset,
            handle: 0,
            size,
        };
        if let Some(ub) = self.uniform_buffer_manager.get_uniform_buffer(handle) {
            binding.handle = ub.buffer.get_handle();
            binding.size = if size != 0 { size } else { ub.buffer.size() };
        }

        self.draw_state_manager.set_uniform_buffer(binding);
    }

    pub fn set_storage_buffer(
        &mut self,
        handle: StorageBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    ) {
        let mut binding = StorageBinding {
            location,
            offset,
            handle: 0,
            size,
        };
        if let Some(ub) = self.storage_buffer_manager.get_storage_buffer(handle) {
            binding.handle = ub.buffer.get_handle();
            binding.size = if size != 0 { size } else { ub.buffer.size() };
        }

        self.draw_state_manager.set_storage_buffer(binding);
    }

    pub fn set_texture(&mut self, tex: TextureHandle, sampler: SamplerHandle, location: u32) {
        let tex = self.texture_manager.get_texture(tex);
        let sampler = self.sampler_manager.get_sampler(sampler);

        let mut binding = TextureBinding {
            handle: 0,
            sampler: 0,
            location,
            ty: TextureType::Single1D,
        };
        if let Some(tex) = tex {
            binding.handle = tex.handle();
            binding.ty = tex.ty();
        }
        if let Some(sampler) = sampler {
            binding.sampler = sampler.handle();
        }
        self.draw_state_manager.set_texture(binding);
    }

    pub fn draw(&self, instances: u32, base_instance: u32) {
        self.draw_state_manager
            .draw(instances as _, base_instance as _);
    }

    fn update_frame(&mut self) {
        self.current_frame = (self.current_frame + 1) % 3;
        self.staging_buffer_manager.update_frame(self.current_frame);
        self.sync_manager.wait_for_sync_object(self.current_frame);
    }
}
