use ::gl;
use gfx_common::config::{MAX_FRAGMENT_SHADERS, MAX_VERTEX_SHADERS};
use gfx_common::{FragmentShaderHandle, VertexShaderHandle};
use log::error;

pub struct ShaderManager {
    vertex_shaders: [Shader; MAX_VERTEX_SHADERS],
    fragment_shaders: [Shader; MAX_FRAGMENT_SHADERS],
}

impl ShaderManager {
    pub fn new() -> Self {
        Self {
            vertex_shaders: [Shader { handle: 0 }; MAX_VERTEX_SHADERS],
            fragment_shaders: [Shader { handle: 0 }; MAX_FRAGMENT_SHADERS],
        }
    }

    pub fn create_vertex_shader(&mut self, data: &[u8]) -> VertexShaderHandle {
        for (i, shader) in (0u16..).zip(self.vertex_shaders.iter()) {
            if shader.is_valid() {
                continue;
            }

            if let Ok(shader) = Shader::create(data, Type::Vertex) {
                self.vertex_shaders[i as usize] = shader;
                return VertexShaderHandle::new(i);
            } else {
                error!("[ShaderManager]: Failed to crate vertex shader");
                return VertexShaderHandle::invalid();
            }
        }
        error!("[ShaderManager]: Reached MAX_VERTEX_SHADERS limit. Can't create a new one");
        VertexShaderHandle::invalid()
    }

    pub fn destroy_vertex_shader(&mut self, handle: VertexShaderHandle) {
        if handle.get_id() > MAX_VERTEX_SHADERS as _ {
            error!(
                "[ShaderManager]: Destroy vertex shader failed: vertex shader index out of bounds"
            );
            return;
        }

        self.vertex_shaders[handle.get_id() as usize].destroy();
        self.vertex_shaders[handle.get_id() as usize] = Shader { handle: 0 };
    }

    pub fn get_vertex_shader(&self, handle: VertexShaderHandle) -> Option<&Shader> {
        if handle.get_id() > MAX_VERTEX_SHADERS as _ {
            error!("[ShaderManager]: Get vertex shader failed: vertex shader index out of bounds");
            return None;
        }

        Some(&self.vertex_shaders[handle.get_id() as usize])
    }

    pub fn create_fragment_shader(&mut self, data: &[u8]) -> FragmentShaderHandle {
        for (i, shader) in (0u16..).zip(self.fragment_shaders.iter()) {
            if shader.is_valid() {
                continue;
            }

            if let Ok(shader) = Shader::create(data, Type::Fragment) {
                self.fragment_shaders[i as usize] = shader;
                return FragmentShaderHandle::new(i);
            } else {
                error!("[ShaderManager]: Failed to crate fragment shader");
                return FragmentShaderHandle::invalid();
            }
        }
        error!("[ShaderManager]: Reached MAX_VERTEX_SHADERS limit. Can't create a new one");
        FragmentShaderHandle::invalid()
    }

    pub fn destroy_fragment_shader(&mut self, handle: FragmentShaderHandle) {
        if handle.get_id() > MAX_VERTEX_SHADERS as _ {
            error!("[ShaderManager]: Destroy fragment shader failed: fragment shader index out of bounds");
            return;
        }

        self.fragment_shaders[handle.get_id() as usize].destroy();
        self.fragment_shaders[handle.get_id() as usize] = Shader { handle: 0 };
    }

    pub fn get_fragment_shader(&self, handle: FragmentShaderHandle) -> Option<&Shader> {
        if handle.get_id() > MAX_VERTEX_SHADERS as _ {
            error!(
                "[ShaderManager]: Get fragment shader failed: fragment shader index out of bounds"
            );
            return None;
        }

        Some(&self.fragment_shaders[handle.get_id() as usize])
    }
}

#[derive(Copy, Clone)]
pub struct Shader {
    handle: u32,
}

impl Shader {
    fn create(data: &[u8], ty: Type) -> Result<Self, ()> {
        let shader_handle = unsafe { gl::CreateShader(ty.to_gl()) };
        let size = data.len() as i32;
        let mut success = 0;
        let ptr = [data.as_ptr()];
        unsafe {
            gl::ShaderSource(shader_handle, 1, ptr.as_ptr() as _, &size as _);
        }
        unsafe {
            gl::CompileShader(shader_handle);
        }
        unsafe {
            gl::GetShaderiv(shader_handle, gl::COMPILE_STATUS, &mut success as _);
        }
        if success == 0 {
            unsafe {
                gl::DeleteShader(shader_handle);
            }
            return Err(());
        }

        let handle = unsafe { gl::CreateProgram() };
        unsafe {
            gl::AttachShader(handle, shader_handle);
        }
        unsafe {
            gl::ProgramParameteri(handle, gl::PROGRAM_SEPARABLE, gl::TRUE as _);
        }
        unsafe {
            gl::LinkProgram(handle);
        }

        unsafe {
            gl::DeleteShader(shader_handle);
        }
        unsafe {
            gl::GetProgramiv(handle, gl::LINK_STATUS, &mut success as _);
        }
        if success == 0 {
            unsafe {
                gl::DeleteProgram(handle);
            }
            return Err(());
        }

        Ok(Self { handle })
    }

    fn destroy(&self) {
        unsafe {
            gl::DeleteProgram(self.handle);
        }
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0
    }

    pub fn get_handle(&self) -> u32 {
        self.handle
    }
}

enum Type {
    Vertex,
    Fragment,
}

impl Type {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Vertex => gl::VERTEX_SHADER,
            Self::Fragment => gl::FRAGMENT_SHADER,
        }
    }
}
