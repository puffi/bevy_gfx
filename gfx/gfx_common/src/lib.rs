use config::{
    MAX_FRAGMENT_SHADERS, MAX_INDEX_BUFFERS, MAX_RENDER_TARGETS, MAX_SAMPLERS, MAX_STORAGE_BUFFERS,
    MAX_TEXTURES, MAX_UNIFORM_BUFFERS, MAX_VERTEX_BUFFERS, MAX_VERTEX_SHADERS,
};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::os::raw::{c_ulong, c_void};

pub mod config;

pub enum PlatformHandle {
    Xlib {
        window: c_ulong,
        display: *mut c_void,
    },
    Wayland {
        surface: *mut c_void,
        display: *mut c_void,
    },
    Win32 {
        hwnd: *mut c_void,
        hdc: *mut c_void,
        hinstance: *mut c_void,
    },
}

pub trait Platform {
    fn get_platform_handle(&self) -> PlatformHandle;
    fn get_window_size(&self) -> (i32, i32);
}

macro_rules! define_handle {
    ($name: ident, $type: ty, $max: expr) => {
        paste::item! {
            #[derive(Copy, Clone, Debug, PartialEq, Eq)]
            pub struct [<$name Handle>] {
                id: $type,
            }

            impl [<$name Handle>] {
                pub fn new(id: $type) -> Self {
                    Self { id }
                }

                pub fn is_valid(&self) -> bool {
                    self.id < $max as _
                }

                pub fn get_id(&self) -> $type {
                    self.id
                }

                pub fn invalid() -> Self {
                    Default::default()
                }
            }

            impl Default for [<$name Handle>] {
                fn default() -> Self {
                    Self {
                        id: std::$type::MAX
                    }
                }
            }
        }
    };
}

define_handle!(RenderTarget, u16, MAX_RENDER_TARGETS);
define_handle!(VertexShader, u16, MAX_VERTEX_SHADERS);
define_handle!(FragmentShader, u16, MAX_FRAGMENT_SHADERS);
define_handle!(UniformBuffer, u16, MAX_UNIFORM_BUFFERS);
define_handle!(StorageBuffer, u16, MAX_STORAGE_BUFFERS);
define_handle!(Texture, u16, MAX_TEXTURES);
define_handle!(Sampler, u16, MAX_SAMPLERS);
define_handle!(VertexBuffer, u16, MAX_VERTEX_BUFFERS);
define_handle!(IndexBuffer, u16, MAX_INDEX_BUFFERS);

pub const DEFAULT_RENDER_TARGET: RenderTargetHandle = RenderTargetHandle { id: 0 };

#[derive(PartialEq, Copy, Clone)]
pub enum PresentMode {
    Immediate,
    VSync,
}

#[derive(PartialEq, Eq, Hash)]
pub enum Attachment {
    Color0,
    Color1,
    Color2,
    Color3,
    Color4,
    Color5,
    Color6,
    Color7,
    Depth,
    Stencil,
    DepthStencil,
}

#[derive(PartialEq)]
pub enum ClearValue {
    F32(f32),
    I32(i32),
    U32(u32),
    Vec4([f32; 4]),
    F32I32(f32, i32),
    None,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum IndexType {
    U16,
    U32,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum TextureType {
    Single1D,
    Single2D,
    Single3D,
    Array1D,
    Array2D,
}

impl std::fmt::Display for TextureType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Single1D => write!(f, "Single1D"),
            Self::Single2D => write!(f, "Single2D"),
            Self::Single3D => write!(f, "Single3D"),
            Self::Array1D => write!(f, "Array1D"),
            Self::Array2D => write!(f, "Array2D"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Format {
    R8,
    RGB8,
    RGBA8,
    RGB16F,
    RGBA16F,
    RGB32F,
    RGBA32F,
    Depth32,
    Depth32Stencil8,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Filter {
    Nearest,
    Linear,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Wrap {
    Repeat,
    MirroredRepeat,
    Clamp,
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Attribute {
    pub location: u8,
    pub ty: AttributeType,
    pub num: u8,
    pub normalize: bool,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum AttributeType {
    None,
    U8,
    F32,
}

impl Default for AttributeType {
    fn default() -> Self {
        Self::None
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Default, PartialEq, Eq, Debug, Clone, Copy)]
pub struct BlendState {
    pub is_enabled: bool,
    pub func: BlendFunc,
    pub equation: BlendEquation,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct BlendFunc {
    pub source: BlendFactor,
    pub destination: BlendFactor,
}

impl Default for BlendFunc {
    fn default() -> Self {
        Self {
            source: BlendFactor::One,
            destination: BlendFactor::Zero,
        }
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BlendFactor {
    Zero,
    One,
    SrcColor,
    OneMinusSrcColor,
    DstColor,
    OneMinusDstColor,
    SrcAlpha,
    OneMinusSrcAlpha,
    DstAlpha,
    OneMinusDstAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SrcAlphaSaturate,
    Src1Color,
    OneMinusSrc1Color,
    // 	src1Alpha,
    OneMinusSrc1Alpha,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BlendEquation {
    Add,
    Subtract,
    ReverseSubtract,
    Min,
    Max,
}

impl Default for BlendEquation {
    fn default() -> Self {
        Self::Add
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct DepthState {
    pub is_enabled: bool,
    pub func: DepthFunc,
}

impl Default for DepthState {
    fn default() -> Self {
        Self {
            is_enabled: true,
            func: DepthFunc::Less,
        }
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum DepthFunc {
    Never,
    Less,
    Equal,
    LEqual,
    Greater,
    NotEqual,
    GEqual,
    Always,
}

impl Default for DepthFunc {
    fn default() -> Self {
        DepthFunc::Less
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct CullState {
    pub is_enabled: bool,
    pub mode: CullMode,
    pub front_face: CullFrontFace,
}

impl Default for CullState {
    fn default() -> Self {
        Self {
            is_enabled: true,
            mode: CullMode::Back,
            front_face: CullFrontFace::Ccw,
        }
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum CullMode {
    Front,
    Back,
    FrontAndBack,
}

impl Default for CullMode {
    fn default() -> Self {
        Self::Back
    }
}

//impl std::fmt::Display for CullMode {
//    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
//        match self {
//            Front => write!(f, "CullMode::Front"),
//            Back => write!(f, "CullMode::Back"),
//            FrontAndBack => write!(f, "CullMode::FrontAndBack"),
//        }
//    }
//}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum CullFrontFace {
    Cw,
    Ccw,
}

impl Default for CullFrontFace {
    fn default() -> Self {
        Self::Ccw
    }
}

//impl std::fmt::Display for CullFrontFace
//{
//    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
//        match self {
//            Cw => write!(f, "CullFrontFace::Cw"),
//            Ccw => write!(f, "CullFrontFace::Ccw"),
//        }
//    }
//}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(PartialEq, Eq, Debug, Clone, Copy, Default)]
pub struct ScissorState {
    pub is_enabled: bool,
    pub rect: [i32; 4],
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Viewport {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Uniform {
    pub location: u32,
    pub handle: UniformBufferHandle,
    pub offset: u32,
    pub size: u32,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Storage {
    pub location: u32,
    pub handle: StorageBufferHandle,
    pub offset: u32,
    pub size: u32,
}

pub trait Vertex {
    const ATTRIBUTES: &'static [Attribute];

    fn get_attributes() -> &'static [Attribute];
}
