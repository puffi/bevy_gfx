use proc_macro::TokenStream;
use quote::quote;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, Data, DeriveInput, Fields, LitBool, LitInt, Result,
};

#[proc_macro_derive(Vertex, attributes(attrib))]
pub fn derive_vertex(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = &input.ident;

    let mut attributes = Vec::new();
    match &input.data {
        Data::Struct(s) => match &s.fields {
            Fields::Named(named) => {
                for field in named.named.iter() {
                    for attr in &field.attrs {
                        match attr.parse_args::<AttributeInput>() {
                            Ok(attribs) => {
                                attributes.push(attribs);
                            }
                            Err(_err) => {}
                        }
                    }
                }
            }
            Fields::Unnamed(_unnamed) => {}
            _ => panic!("Unit struct can't derive Vertex"),
        },
        _ => panic!("Only structs can derive Vertex"),
    }
    let mut stream = proc_macro2::TokenStream::new();
    let attrib_iter = attributes.iter();
    for attr in attrib_iter {
        let loc = &attr.loc_literal;
        let ty = &attr.ty_literal;
        let num = &attr.num_literal;
        let normalize = &attr.normalize_literal;
        stream.extend(quote! {
            Attribute {
                location: #loc,
                ty: AttributeType::#ty,
                num: #num,
                normalize: #normalize,
            },
        });
    }
    let output = quote! {
        use gfx_common::{Attribute, AttributeType, Vertex};

        impl Vertex for #name {

            const ATTRIBUTES: &'static [Attribute] = &[ #stream ];

            fn get_attributes() -> &'static [Attribute] {
                Self::ATTRIBUTES
            }
        }
    };
    output.into()
}

#[derive(Clone, Debug)]
struct AttributeInput {
    loc_literal: LitInt,
    ty_literal: syn::Variant,
    num_literal: LitInt,
    normalize_literal: LitBool,
}

impl Parse for AttributeInput {
    fn parse(input: ParseStream) -> Result<Self> {
        let _ = input.parse::<syn::Ident>()?;
        input.parse::<syn::Token![=]>()?;
        let loc_literal = input.parse()?;
        input.parse::<syn::Token![,]>()?;
        let _ = input.parse::<syn::Ident>()?;
        input.parse::<syn::Token![=]>()?;
        let ty_literal = input.parse()?;
        input.parse::<syn::Token![,]>()?;
        let _ = input.parse::<syn::Ident>()?;
        input.parse::<syn::Token![=]>()?;
        let num_literal = input.parse()?;
        input.parse::<syn::Token![,]>()?;
        let _ = input.parse::<syn::Ident>()?;
        input.parse::<syn::Token![=]>()?;
        let normalize_literal = input.parse()?;

        Ok(AttributeInput {
            loc_literal,
            ty_literal,
            num_literal,
            normalize_literal,
        })
    }
}
