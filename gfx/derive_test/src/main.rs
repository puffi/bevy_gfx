fn main() {
    println!("Hello, world!");
}

use gfx_derive::*;

#[derive(Vertex)]
struct Vertex3D {
    #[attrib(buffer = 0, location = 0, ty = F32, num = 3, normalize = false)]
    _pos: [f32; 3],
}
